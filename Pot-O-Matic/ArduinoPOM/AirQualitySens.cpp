#include "GalTam.h"
#include "AirQualitySens.h"
#include "SparkFun_SGP30_Arduino_Library.h"

static SGP30 airQuality;
AirQualitySens AirQuality;

void AirQualitySens::Initialize()
  {
  co2 = tvoc = last_reported_co2 = last_reported_tvoc = 0;
  blueCO2 = new Characteristic(0, SSC_CO2, "CO2", NULL, BLERead | BLENotify);
  blueTVOC = new Characteristic(0, SSC_TVOC, "TVOC", NULL, BLERead | BLENotify);
  }

void AirQualitySens::SystemEvent(SystemEventType type, int n)
  {
  if (type == SE_PC_Reconnect)
    hostDirty = true;
  }

void AirQualitySens::Tick()
  {
  char scr[50];
  static bool lastCom = false, gotSensor = false;

  if (gotSensor)
    {
    gotSensor = airQuality.measureAirQuality() == SGP30_SUCCESS;
    if (gotSensor)
      {
      co2 = airQuality.CO2 - 350;
      tvoc = airQuality.TVOC;
      }
    }
  else
    co2 = tvoc = 0;

  if (co2 != last_reported_co2 || tvoc != last_reported_tvoc)
    {
    hostDirty = true;
    blueCO2->Write(co2);
    blueTVOC->Write(tvoc);
    last_reported_co2 = co2;
    last_reported_tvoc = tvoc;
    }

  if (HostProtocol::Communicating() && co2 && (hostDirty || !lastCom))
    {
    sprintf(scr, "AQ|CO%d|TV%d", co2, tvoc);
    HostProtocol::Send(scr);
    hostDirty = false;
    }

  if (!gotSensor)
    {
    gotSensor = airQuality.begin();
    if (gotSensor)
      airQuality.initAirQuality();
    }
  lastCom = HostProtocol::Communicating();
  }

void AirQualitySens::SetHumidity(USHORT val)
  {
  airQuality.setHumidity(val);
  }
