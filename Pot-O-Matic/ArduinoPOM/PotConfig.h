//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#ifndef POTCONFIG_H
#define POTCONFIG_H

enum PotCharacteristic
  {
  POT_Name = 3,
  POT_PumpDuration,
  POT_WateringInterval,
  POT_MoistureThreshold,
  POT_MoistureReading,
  POT_NeedToWater,
  POT_SecsTilWater,
  POT_PumpOnRemainingSeconds,
  POT_MoistureReadingPct,
  POT_MoistureWetReading,
  POT_MoistureDryReading,
  };

#endif
