// GalTam.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//
//  Author: Greg Winters
//

#ifndef GALTAM_H
#define GALTAM_H

#define K                   * 1024

#define SYSTEM_TPS          1000				// System ticks per second
#define	TICKS_PER_SECOND	SYSTEM_TPS
#define SECONDS             * TICKS_PER_SECOND
#define SECOND              SECONDS
#define MINUTES             SECONDS * 60
#define MINUTE              MINUTES
#define HOURS               MINUTES * 60
#define DAYS                HOURS * 24
#define TENTHS              * (TICKS_PER_SECOND / 10)
#define TENTH               TENTHS

#define SECONDS_IN_uSEC  *1000000UL
#define MILLISECONDS_IN_uSEC  * 1000UL
#define MINUTES_IN_uSEC  SECONDS_IN_uSEC * 60

#define __max(a,b) (((a) > (b)) ? (a) : (b))
#define __min(a,b) (((a) < (b)) ? (a) : (b))

#define arraysize(a)    (sizeof(a) / sizeof((a)[0]))

#define IN_FLASH(x)     ((ULONG)x <= 0xFFFFF)

typedef unsigned char UC;
typedef unsigned char UCHAR;

typedef short SHORT;
typedef unsigned short USHORT;

typedef unsigned long ULONG;

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

#define	FOREVER			for (;;)
#define TwoChar(x, y)   ((x << 8) + y)

/// Specifies what order functions will be executed
enum OrderOfExecution
{
    OE_First,
    OE_Early,
    OE_Middle,
    OE_Late,
    OE_Last
};

// Everybody needs these...
#include "StrOp.h"
#include "Obj.h"
#include "List.h"
#include "Conf.h"
#include "Ticker.h"
#include "HostCommand.h"
#include "HostProtocol.h"
#include "Logger.h"
#include "BLE.h"

#endif
