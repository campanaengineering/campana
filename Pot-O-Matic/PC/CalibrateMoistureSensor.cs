﻿using System;
using System.Text;
using System.Windows.Forms;

namespace Pot_O_Matic_Interface
{
    public partial class CalibrateMoistureSensor : Form
    {
        #region Variables

        private int pot = -1;

        private int reading = -1;

        private PotDef[] Pot = UI.Pot;

        private ArduinoCom POM = UI.POM;

        private const int MaxDelta = 60;

        private string selectText = string.Empty;

        #endregion

        #region Start/Stop

        public CalibrateMoistureSensor()
        {
            InitializeComponent();

            POM.ArduinoEvent += Pom_ArduinoEvent;

            selectText = SelectedPot.Text = "Choose pot to configure";
            SelectedPot.Items.Add(selectText);
            for (int p = 0; p < UI.MaxPot; p++)
                SelectedPot.Items.Add(Pot[p].PotGroup.Text);
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            POM.ArduinoEvent -= Pom_ArduinoEvent;
            base.OnFormClosing(e);
        }

        #endregion

        #region Events

        private void Pom_ArduinoEvent(ArduinoEventType type, object obj)
        {
            #region Invoke

            try
            {
                if (InvokeRequired)
                {
                    Invoke(new UI.Pom_ArduinoEvent_Callback(Pom_ArduinoEvent), new object[] { type, obj });
                    return;
                }
            }
            catch (Exception)
            {
            }

            #endregion

            switch (type)
            {
            case ArduinoEventType.ArduinoConnected:
                if (!(bool)obj)
                    Close();
                break;

            case ArduinoEventType.StatusUpdate:
                {
                    int tmp = 0;
                    string msg = (string)obj;

                    switch (msg.Substring(0, 2))
                    {
                    case "MR":      // Moisture
                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "MR":
                                if (pot != Utility.Parse.AtoI(token.Substring(2)))
                                    return;
                                break;

                            case "RE":
                                tmp = Utility.Parse.AtoI(token.Substring(2));
                                if (tmp > reading)
                                    tmp = (tmp - reading > MaxDelta) ? tmp : reading;
                                reading = tmp;

                                Reading.Text = string.Format("Reading: {0}", reading < 0 ? "???" : reading.ToString());
                                break;
                            }
                        break;
                    }
                }
                break;
            }
        }

        #endregion

        #region UI Events

        private void BeginCal_Click(object sender, EventArgs e)
        {
            POM.UpdateMoistureConfiguration(pot, reading);
            Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SelectedPot_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedPot.SelectedIndex > 0)
            {
                BeginCal.Enabled = true;
                pot = SelectedPot.SelectedIndex - 1;
                Reading.Text = string.Format("Reading: {0}", UI.LastReading[pot].ToString());
            }
            else
            {
                pot = -1;
                BeginCal.Enabled = false;
                SelectedPot.Text = selectText;
            }
        }

        #endregion
    }
}
