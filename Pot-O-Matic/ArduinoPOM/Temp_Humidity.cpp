#include "GalTam.h"
#include "AirQualitySens.h"
#include "SparkFun_SHTC3.h"

/// Class to provide access to I2C Mux
class TempHumidity : public Ticker
{
public:
  TempHumidity() : Ticker("TempHumidity", 1), lastHumid(0), lastTempInt(0), lastHumidInt(0) { }

private:
  virtual void Tick();
  virtual void Initialize();
  virtual void SystemEvent(SystemEventType type, int n);

  USHORT DoubleToFixedPoint(double number);
  double RelativeToAbsolute(float relHumidity, float tempC);

  USHORT lastHumid;
  USHORT lastTempInt;
  USHORT lastHumidInt;
  SHTC3 humiditySensor;
  Characteristic *blueTemp;
  Characteristic *blueHumidity;
  float last_reported_humidity, last_reported_temperature;

  static HardInit *hInit;                     // Hardware initialization
  static void HardwareInitialize();
} th;


HardInit *TempHumidity::hInit = new HardInit(HardwareInitialize);
void TempHumidity::HardwareInitialize()
  {
  Wire.begin();
  Wire.setClock(400000);
  }

void TempHumidity::Initialize()
  {
  last_reported_humidity = last_reported_temperature = 0;
  blueTemp = new Characteristic(0, SSC_Temperature, "Temperature", NULL, BLERead | BLENotify);
  blueHumidity = new Characteristic(0, SSC_Humidity, "Humidity", NULL, BLERead | BLENotify);
  }

void TempHumidity::SystemEvent(SystemEventType type, int n)
  {
  if (type == SE_PC_Reconnect)
    {
    last_reported_humidity = last_reported_temperature = 0;
    Tick();
    }
  }

void TempHumidity::Tick()
  {
  char scr[50];
  double absHumidity;
  USHORT sensHumidity;
  float humidity, temperature;
  static bool lastCom = false;
  bool tempDirty = false, humidDirty = false;
  static int sensStatus = SHTC3_Status_Error;

  if (sensStatus == SHTC3_Status_Nominal)
    {
    humidity = humiditySensor.toPercent();
    temperature = humiditySensor.toDegF();

    //Convert relative humidity to absolute humidity
    absHumidity = RelativeToAbsolute(humiditySensor.toPercent(), humiditySensor.toDegC());

    //Convert the double type humidity to a fixed point 8.8bit number
    sensHumidity = DoubleToFixedPoint(absHumidity);

    //Set the humidity compensation on the SGP30 to the measured value
    //If no humidity sensor attached, sensHumidity should be 0 and sensor will use default
    if (sensHumidity != lastHumid)
      AirQuality.SetHumidity(sensHumidity);
    lastHumid = sensHumidity;
    }
  else
    temperature = humidity = 0.0;
  
  if (abs(temperature - last_reported_temperature) >= 1.0)
    {
    tempDirty = true;
    blueTemp->Write((int)temperature);
    last_reported_temperature = temperature;
    }

  if (abs(humidity - last_reported_humidity) >= 1.0)
    {
    humidDirty = true;
    last_reported_humidity = humidity;
    blueHumidity->Write((int)humidity);
    }

  if (HostProtocol::Communicating() && temperature && (tempDirty || !lastCom))
    {
    sprintf(scr, "TP%d", (int)temperature);
    HostProtocol::Send(scr);
    Log(LC_Orange, false, "Temperature: %d", (int)temperature);
    }

  if (HostProtocol::Communicating() && humidity && (humidDirty || !lastCom))
    {
    sprintf(scr, "HU%d", (int)humidity);
    HostProtocol::Send(scr);
    Log(LC_Orange, false, "Humidity: %d", (int)humidity);
    }

  if (sensStatus != SHTC3_Status_Nominal)
    sensStatus = humiditySensor.begin();
  if (sensStatus == SHTC3_Status_Nominal)
    sensStatus = humiditySensor.update();

  lastCom = HostProtocol::Communicating();
  }

double TempHumidity::RelativeToAbsolute(float relHumidity, float tempC)
  {
  double eSat = 6.11 * pow(10.0, (7.5 * tempC / (237.7 + tempC)));
  double vaporPressure = (relHumidity * eSat) / 100; //millibars
  double absHumidity = 1000 * vaporPressure * 100 / ((tempC + 273) * 461.5); //Ideal gas law with unit conversions
  return absHumidity;
  }

USHORT TempHumidity::DoubleToFixedPoint(double number)
  {
  int power = 1 << 8;
  double number2 = number * power;
  USHORT value = floor(number2 + 0.5);
  return value;
  }
