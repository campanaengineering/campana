﻿#region Using

using System;
using System.Threading;

#endregion

namespace Pot_O_Matic_Interface
{
    #region Enumerations

    public enum ArduinoEventType
    {
        Log,
        Exception,
        DataScope,
        StatusUpdate,
        ArduinoConnected,

        Max
    }

    #endregion

    public class ArduinoCom : IDisposable
    {
        #region Variables

        private Executive exec = null;

        public bool Simulated { get; set; } = false;

        public static ArduinoCom Instance { get; private set; }

        public string Version { get; internal set; } = string.Empty;

        public string Hardware { get; internal set; } = string.Empty;

        public string DeviceName { get; internal set; } = string.Empty;

        public int Checksum { get; internal set; } = 0;

        #endregion

        #region Startup/Shutdown

        public ArduinoCom()
        {
            Instance = this;
            exec = new Executive();
            new WorkerThread(ArduinoEventThread);
        }

        /// <summary>
        /// Destructor for ArduinoIO
        /// </summary>
        ~ArduinoCom()
        {
            Dispose();
        }

        /// <summary>
        /// Dispose of object
        /// </summary>
        public void Dispose()
        {
            if (exec != null && exec.Communicating)
                GoingDown();

            Instance = null;
            if (exec != null)
                exec.Dispose();
            exec = null;
        }

        #endregion

        #region Events

        /// <summary>
        /// Device event
        /// </summary>
        /// <param name="type">Event type</param>
        /// <param name="obj">Data for event</param>
        public delegate void ArduinoEventDelegate(ArduinoEventType type, object obj);
        public event ArduinoEventDelegate ArduinoEvent;
        public MessageQueue<ArduinoEventContainer> ArduinoEventQueue = new MessageQueue<ArduinoEventContainer>(100);


        public void SendEvent(ArduinoEventType type, object obj = null)
        {
            ArduinoEventQueue.Enqueue(new ArduinoEventContainer(type, obj));
        }

        private void ArduinoEventThread(object ignored)
        {
            Thread.Sleep(1000);        // Allow time to attach to events

            while (exec != null)
            {
                try
                {           // During shutdown, 'exec' may go null at any instant.  Protect code if happens while servicing event
                    if (ArduinoEventQueue.Sem.WaitOne(1000) && exec != null)
                    {
                        ArduinoEventContainer ev = ArduinoEventQueue.Dequeue();

                        if (ev != null)
                            ArduinoEvent?.Invoke(ev.Event, ev.Obj);
                    }
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion

        #region Commands

        public void SetDeviceName(string name)
        {
            exec.SendCommand(string.Format("DN{0}", name));
        }

        public void SetPotName(int p, string name)
        {
            exec.SendCommand(string.Format("PN{0}|NM{1}", p, name));
        }

        public void ForceWatering(int p)
        {
            if (p >= 0 && p < UI.MaxPot)
                exec.SendCommand(string.Format("FW{0}", p));
        }

        public void UpdateWaterDuration(int p, int d)
        {
            if (p >= 0 && p < UI.MaxPot)
                exec.SendCommand(string.Format("WD{0}|NM{1}", p, d));
        }

        public void UpdateThreshold(int p, int th)
        {
            if (p >= 0 && p < UI.MaxPot)
                exec.SendCommand(string.Format("MT{0}|TH{1}", p, th));
        }

        public void UpdateSimulatedReading(int p, int re)
        {
            if (p >= 0 && p < UI.MaxPot)
                exec.SendCommand(string.Format("MV{0}|RE{1}", p, re));
        }

        public void UpdateWateringInterval(int p, int d)
        {
            if (p >= 0 && p < UI.MaxPot)
                exec.SendCommand(string.Format("WI{0}|NM{1}", p, d));
        }

        public void UpdateMoistureConfiguration(int p, int w)
        {
            if (p >= 0 && p < UI.MaxPot)
                exec.SendCommand(string.Format("MP{0}|WT{1}", p, w));
        }

        /// <summary>
        /// Sent when closing down communication gracefully
        /// </summary>
        public void GoingDown()
        {
            exec.SendCommand("GD", true);
        }

        #endregion
    }

    #region ArduinoEventContainer

    /// <summary>
    /// Container for a Arduino Event
    /// </summary>
    public class ArduinoEventContainer
    {
        /// <summary>
        /// The event type
        /// </summary>
        public ArduinoEventType Event { get; private set; }

        /// <summary>
        /// Any associated generic object
        /// </summary>
        public object Obj { get; private set; }

        /// <summary>
        /// Create a Funnel Event
        /// </summary>
        /// <param name="ev">Event type</param>
        /// <param name="text">Any associated text</param>
        /// <param name="obj">Any associated generic object</param>
        internal ArduinoEventContainer(ArduinoEventType ev, object obj = null)
        {
            Obj = obj;
            Event = ev;
        }
    }

    #endregion

    #region Worker Thread

    internal class WorkerThread
    {
        public WaitCallback Function;

        public Object Parameter;

        public WorkerThread(WaitCallback func, Object param = null)
        {
            Function = func;
            Parameter = param;

            ThreadPool.QueueUserWorkItem(new WaitCallback(ThreadShell));
        }

        /// <summary>
        /// Shell to execute thread function that catches exceptions and logs them
        /// </summary>
        /// <param name="obj">Object that contains a ThreadParameters instance</param>
        private void ThreadShell(Object obj)
        {
            try
            {
                Function(Parameter);
            }
            catch (Exception e)
            {
                if (ArduinoCom.Instance != null)
                    ArduinoCom.Instance.SendEvent(ArduinoEventType.Exception, e);
            }
        }
    }

    #endregion
}
