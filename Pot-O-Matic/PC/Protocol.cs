﻿#region Using

using System;
using System.Text;
using Microsoft.Win32;
using System.IO.Ports;
using System.Threading;
using System.Collections.Generic;

#endregion

namespace Pot_O_Matic_Interface
{
    #region Enumerations

    /// <summary>
    /// Protocol application states
    /// </summary>
    public enum AppStateType
    {
        Unknown,
        Down,
        AppReady,
    }

    /// <summary>
    /// Level of datascope logging
    /// </summary>
    public enum DataScopeLevel
    {
        None,
        Packets,
    }

    /// <summary>
    /// Protocol event type
    /// </summary>
    public enum ProtocolEventType
    {
        Log,
        DataScope,
        AppStateChanged,
        MessageReceived,
    }

    /// <summary>
    /// The supported protocol characters
    /// </summary>
    public enum ProtChar
    {
        STX = 0x2,
        ETX = 0x3,
        ENQ = 0x5,
    }

    #endregion

    /// <summary>
    /// Protocol handler
    /// </summary>
    public class Protocol : IDisposable
    {
        #region Variables

        /// <summary>
        /// Which COM port to use
        /// </summary>
        public static string ComPort = null;

        /// <summary>
        /// Datascope level
        /// </summary>
        public static DataScopeLevel DataScope { get; set; } = DataScopeLevel.None;

        /// <summary>
        /// Keep track of open error being reported
        /// </summary>
        private bool _openErrorReported = false;

        /// <summary>
        /// Protocol events sent here
        /// </summary>
        public event ProtocolEventDelegate ProtocolEvent;

        /// <summary>
        /// Delegate for Protocol events
        /// </summary>
        /// <param name="type">Type of event</param>
        /// <param name="msg">Object associated with event (null if not needed)</param>
        public delegate void ProtocolEventDelegate(ProtocolEventType type, string msg);

        /// <summary>
        /// True if protocol temporarily paused
        /// </summary>
        private bool _protocolPaused = false;

        /// <summary>
        /// When last good packet received
        /// </summary>
        DateTime lastGoodRxPacket = DateTime.Now;

        /// <summary>
        /// True while read is paused
        /// </summary>
        private bool _readPaused = false;

        /// <summary>
        /// True while protocol thread should keep running
        /// </summary>
        private bool _stayAlive = true;

        /// <summary>
        /// The serial port
        /// </summary>
        private static SerialPort _port = null;
        public static bool PortStillOpen { get { return _port != null; } }

        /// <summary>
        /// Maximum receiver buffer
        /// </summary>
        private const int MaxRxSize = 1024;

        /// <summary>
        /// One second of milliseconds
        /// </summary>
        private const int OneSecond = 1000;

        /// <summary>
        /// True while Tx thread running
        /// </summary>
        private bool _txThreadRunning = false;

        /// <summary>
        /// Event set when protocol thread to be terminated
        /// </summary>
        private AutoResetEvent _terminateProtocolThread = new AutoResetEvent(false);

        /// <summary>
        /// The transmit queue
        /// </summary>
        private MessageQueue<string> _txqueue = new MessageQueue<string>();

        /// <summary>
        /// True if Tx queue is empty
        /// </summary>
        public bool TxEmpty { get { return _txqueue.Empty; } }

        /// <summary>
        /// String to send to ping the device
        /// </summary>
        private string PingChar = ENQ.ToString();

        /// <summary>
        /// Build transmit packet using this
        /// </summary>
        private StringBuilder _packetBuilder = new StringBuilder();

        /// <summary>
        /// Protocol character
        /// </summary>
        public const char STX = '\x2';

        /// <summary>
        /// Protocol character
        /// </summary>
        public const char ETX = '\x3';

        /// <summary>
        /// Protocol character
        /// </summary>
        public const char ENQ = '\x5';

        #endregion

        #region Startup/Shutdown

        /// <summary>
        /// Protocol constructor
        /// </summary>
        public Protocol()
        {
        }

        /// <summary>
        /// Destructor for Protocol
        /// </summary>
        ~Protocol()
        {
            Dispose();
        }

        /// <summary>
        /// Start up protocol handler
        /// </summary>
        public void Start()
        {
            ThreadPool.QueueUserWorkItem(new WaitCallback(ReceiveThread), null);
        }

        /// <summary>
        /// Dispose the object
        /// </summary>
        public void Dispose()
        {
            _terminateProtocolThread.Set();
        }

        #endregion

        #region Send Message

        /// <summary>
        /// Send a message to the Funnel
        /// </summary>
        /// <param name="msg">Message to send</param>
        /// <returns>True if message sent and acknowleged</returns>
        public bool SendMessage(string msg)
        {
            if (_protocolPaused)
            {
                Log("Protocol: Message discarded while protocol paused.");
                return false;
            }

            // Build packet
            _packetBuilder.Append(STX);
            _packetBuilder.Append(msg);
            _packetBuilder.Append(ETX);

                        // Log to datascope
            if (ProtocolEvent != null && DataScope == DataScopeLevel.Packets)
                ProtocolEvent(ProtocolEventType.DataScope, "Tx: " + msg);

            // Actually send message
            _txqueue.Enqueue(_packetBuilder.ToString());
            _packetBuilder.Clear();

            return false;
        }

        #endregion

        #region Receive Thread

        /// <summary>
        /// Thread to get data from serial port and parse it into packets
        /// </summary>
        /// <param name="ignored">Thread parameter not used</param>
        private void ReceiveThread(Object ignored)
        {
            Byte chr;
            StringBuilder packetReceived = new StringBuilder();

            _stayAlive = true;
            while (_stayAlive)
            {
                InitializePort();

                if (!_txThreadRunning)
                {
                    _txThreadRunning = true;
                    ThreadPool.QueueUserWorkItem(new WaitCallback(TransmitThread), null);
                }

                while (_stayAlive)
                {
                    if (_port == null || !_port.IsOpen)
                    {
                        if (!InitializePort())
                        {
                            AppState = AppStateType.Down;
                            Thread.Sleep(500);
                        }
                        continue;
                    }

                    while (_stayAlive)
                    {
                        if (_readPaused)
                        {
                            Thread.Sleep(OneSecond / 10);
                            continue;
                        }
                        try { chr = (byte)_port.ReadByte(); }            // Get a byte, recover from all errors
                        catch (TimeoutException)
                        {
                            AppState = AppStateType.Down;
                            continue;
                        }
                        catch (Exception)
                        {
                            Thread.Sleep(OneSecond / 10);
                            break;
                        }

                        switch ((char)chr)
                        {
                        case STX:               // Start of new packet
                            packetReceived.Clear();
                            break;

                        case ETX:               // End of packet
                            if (packetReceived.Length > 1)
                            {               // Have received all of packet.  Process packet
                                string packet = packetReceived.ToString();

                                packetReceived.Clear();
                                lastGoodRxPacket = DateTime.Now;
                                AppState = AppStateType.AppReady;

                                if (ProtocolEvent != null)
                                {
                                    if (DataScope == DataScopeLevel.Packets && !packet.StartsWith("HI"))
                                        ProtocolEvent(ProtocolEventType.DataScope, "Rx: " + packet);
                                    ProtocolEvent(ProtocolEventType.MessageReceived, packet);
                                }
                            }
                            else
                                packetReceived.Clear();
                            break;

                        default:
                            if (packetReceived.Length > MaxRxSize)
                                packetReceived.Clear();
                            else
                                packetReceived.Append((char)chr);
                            break;
                        }
                    }
                }
                try
                {
                    _port.Dispose();
                    _port = null;
                }
                catch (Exception)
                {
                }
            }
        }

        #endregion

        #region Transmit Thread

        /// <summary>
        /// The transmit thread
        /// </summary>
        /// <param name="ignored">Thread parameter unused</param>
        private void TransmitThread(Object ignored)
        {
            WaitHandle[] waitList =
            {
                _txqueue.Sem,                       // Send this message to Ansel
                _terminateProtocolThread,           // End thread when set
            };

            Thread.Sleep(OneSecond / 2);
            if (_port != null && _port.IsOpen)
                _port.Write(PingChar);

            for (; ;)
            {
                switch (WaitHandle.WaitAny(waitList, OneSecond * 10))
                {
                case 0:                                     // Send this message
                    try
                    {
                        string msg = _txqueue.Dequeue();

                        if (_port != null && _port.IsOpen)
                            _port.Write(msg);
                    }
                    catch (Exception) { break; }
                    break;

                case 1:                                     // End thread, causes receiver thread to end also
                    _stayAlive = false;
                    _txThreadRunning = false;
                    return;

                case WaitHandle.WaitTimeout:                // Timeout - Ping if no traffic occurred
                    if (_port != null && _port.IsOpen)
                        _port.Write(PingChar);
                    break;
                }
            }
        }

        #endregion

        #region Tools

        /// <summary>
        /// Cause protocol to be paused
        /// </summary>
        public void PauseProtocol()
        {
            if (!_protocolPaused)
            {
                _protocolPaused = true;
                AppState = AppStateType.Down;
                _terminateProtocolThread.Set();
            }
        }

        /// <summary>
        /// Restart protocol
        /// </summary>
        /// <returns>True if protocol was paused</returns>
        public bool RestartProtocol()
        {
            bool rc = _protocolPaused;

            _protocolPaused = false;
            ThreadPool.QueueUserWorkItem(new WaitCallback(ReceiveThread), null);

            return rc;
        }

        public static string FindArduinoPort()
        {
            try
            {       // Find "SERIAL" ports currently connected to PC
                List<string> portList = new List<string>();

                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DEVICEMAP\SERIALCOMM"))
                    if (key != null && key.GetValueNames() != null)
                        foreach (string subValueName in key.GetValueNames())
                            if (subValueName.ToUpper().Contains("SERIAL"))
                                portList.Add(((string)key.GetValue(subValueName)).ToUpper());

                    // Look through list of CH340E VCP ports that have been connected and compare with currently connected "SERIAL" ports
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Enum\USB\VID_1A86&PID_7523"))
                    if (key != null)
                        foreach (string subKeyName in key.GetSubKeyNames())
                            if (subKeyName != null)
                                using (RegistryKey subKey = key.OpenSubKey(subKeyName))
                                using (RegistryKey parmKey = subKey.OpenSubKey("Device Parameters"))
                                    if (parmKey != null)
                                        try
                                        {
                                            string name = ((string)parmKey.GetValue("PortName")).ToUpper();
                                            if (portList.Contains(name))
                                                return name;
                                        }
                                        catch (Exception)
                                        {
                                        }
            }
            catch (Exception)
            {
            }
            return null;
        }

        /// <summary>
        /// Initialize COM port at specified baud rate
        /// </summary>
        /// <param name="baudRate">Baud rate (0 if shut down port)</param>
        public bool InitializePort()
        {
            if (_port != null)
            {
                _port.Close();
                Thread.Sleep(OneSecond / 3);
                _port.Dispose();
            }

            _readPaused = true;
            ComPort = FindArduinoPort();
            _port = (ComPort != null) ? new SerialPort(ComPort, 115200, Parity.None, 8, StopBits.One) : null;

            if (_port != null)
            {
                for (int i = 0; i <= 10; i++)
                {
                    try
                    {
                        _port.RtsEnable = false;
                        _port.DtrEnable = false;
                        _port.WriteTimeout = OneSecond;
                        _port.Handshake = Handshake.None;
                        _port.ReadTimeout = OneSecond * 15;
                        _port.Open();
                        _port.DiscardInBuffer();

                        _readPaused = false;
                        _openErrorReported = false;
                        return true;
                    }
                    catch (UnauthorizedAccessException)
                    {                       // Sometimes you get this if reopening port too quickly
                        Log("Protocol: Access error during port open (recovered)");
                        Thread.Sleep(OneSecond / 5);
                        continue;
                    }
                    catch (Exception e)
                    {
                        if (!_openErrorReported)
                            Log(string.Format("Protocol: Exception during port open: {0}", e.Message));
                        _openErrorReported = true;
                    }
                    break;
                }
                _port.Close();
                _port = null;
            }
            _readPaused = false;
            return false;
        }

        /// <summary>
        /// Log some text
        /// </summary>
        /// <param name="text">Text to log</param>
        private void Log(string text)
        {
            if (ProtocolEvent != null)
                ProtocolEvent(ProtocolEventType.Log, text);
        }

        /// <summary>
        /// True if Arduino is connected
        /// </summary>
        public bool Connected { get { return (_port != null && _port.IsOpen); } }

        /// <summary>
        /// Static access to protocol AppState
        /// </summary>
        public static AppStateType StaticAppState { get { return _appState; } }

        /// <summary>
        /// Get/Set current application state
        /// </summary>
        public AppStateType AppState
        {
            get { return _appState; }

            private set
            {
                if (value != _appState)
                {
                    _appState = value;

                    //Log(string.Format("AppState changed to {0}", value));

                    if (ProtocolEvent != null)
                        ProtocolEvent(ProtocolEventType.AppStateChanged, null);
                }
            }
        }
        private static AppStateType _appState = AppStateType.Unknown;

        #endregion
    }
}
