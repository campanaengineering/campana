﻿#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

#endregion

namespace Pot_O_Matic_Interface
{
    public partial class UI : Form
    {
        #region Variables

        public const int MaxPot = 2;

        public bool BlockUpdate = false;

        public static UI Instance = null;

        public static ArduinoCom POM = null;

        private string FormName = string.Empty;

        public static int[] LastReading = new int[MaxPot];

        public static PotDef[] Pot = new PotDef[MaxPot];

        #endregion

        #region Startup/Shutdown
        public UI()
        {
            InitializeComponent();

            Instance = this;
            FormName = Text;
            POM = new ArduinoCom();
            POM.ArduinoEvent += Pom_ArduinoEvent;

            DeviceName.Text = Version.Text = Hardware.Text = Checksum.Text = CO2.Text = TVOC.Text = Humidity.Text = Temperature.Text = string.Empty;
            Pot[0] = new PotDef(PotGroup1, CurrentMoisture1, WaterDuration1, WateringInterval1, TargetMoisture1, SecsTilWater1, Watering1, ForceWater1, SimMoisture1);
            Pot[1] = new PotDef(PotGroup2, CurrentMoisture2, WaterDuration2, WateringInterval2, TargetMoisture2, SecsTilWater2, Watering2, ForceWater2, SimMoisture2);

            for (int p = 0; p < MaxPot; p++)
            {
                Pot[p].PotGroup.Text = string.Empty;
                Pot[p].SecsTilWater.Text = string.Empty;
            }
        }

        void CloseDeviceConnection()
        {
            POM.ArduinoEvent -= Pom_ArduinoEvent;
            POM.Dispose();
            POM = null;
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            CloseDeviceConnection();
            base.OnFormClosing(e);
        }

        #endregion

        #region Events

        public delegate void Pom_ArduinoEvent_Callback(ArduinoEventType type, object obj);

        private void Pom_ArduinoEvent(ArduinoEventType type, object obj)
        {
            int pot = -1, tmp = 0;

            #region Invoke

            try
            {
                if (InvokeRequired)
                {
                    Invoke(new Pom_ArduinoEvent_Callback(Pom_ArduinoEvent), new object[] { type, obj });
                    return;
                }
            }
            catch (Exception)
            {
            }

            #endregion

            switch (type)
            {
            case ArduinoEventType.ArduinoConnected:
                if ((bool)obj)
                {       // Came up
                    for (int p = 0; p < MaxPot; p++)
                    {
                        Pot[p].PotGroup.Visible = ChangeAllNames.Visible = CalSensor.Visible = true;
                        if (POM.Simulated)
                        {
                            Pot[p].SimMoisture.Visible = true;
                            POM.UpdateSimulatedReading(p, (int)Pot[p].SimMoisture.Value);
                        }
                    }

                    BlockUpdate = true;
                    DeviceName.Text = ArduinoCom.Instance.DeviceName;
                    Version.Text = string.Format("Version: {0}", ArduinoCom.Instance.Version);
                    Hardware.Text = string.Format("Hardware: {0}", ArduinoCom.Instance.Hardware);
                    Checksum.Text = string.Format("Checksum: {0}", ArduinoCom.Instance.Checksum);
                    Text = FormName + ((POM.Simulated) ? " - Simulated moisture readings" : string.Empty);
                    BlockUpdate = false;
                }
                else
                {       // Went down
                    for (int p = 0; p < MaxPot; p++)
                        Pot[p].PotGroup.Visible = Pot[p].SimMoisture.Visible = false;

                    ChangeAllNames.Visible = CalSensor.Visible = false;
                    Version.Text = Hardware.Text = Checksum.Text = CO2.Text = TVOC.Text = Humidity.Text = Temperature.Text = string.Empty;
                }
                break;

            case ArduinoEventType.Log:
                Log((string)obj);
                break;

            case ArduinoEventType.DataScope:
                Log(((string)obj).Replace('~', ' '), Color.DimGray);
                break;

            case ArduinoEventType.Exception:
                LogException((Exception)obj);
                break;

            case ArduinoEventType.StatusUpdate:
                {
                    string msg = (string)obj;

                    switch (msg.Substring(0, 2))
                    {
                    case "LG":      // Log item from board
                        {
                        bool big = false;
                        Color color = Color.Black;

                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "CO":
                                switch (token.Substring(2))
                                {
                                case "green":
                                    color = Color.Green;
                                    break;

                                case "orange":
                                    color = Color.Chocolate;
                                    break;

                                case "red":
                                    color = Color.Red;
                                    break;

                                case "blue":
                                    color = Color.Blue;
                                    break;
                                }
                                break;

                            case "BG":
                                big = true;
                                break;

                            case "TX":
                                Log(token.Substring(2), color, big);
                                break;
                            }
                        }
                        break;

                    case "PN":      // Pot Name
                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "PN":
                                pot = Utility.Parse.AtoI(token.Substring(2));
                                break;

                            case "NM":
                                Pot[pot].PotGroup.Text = token.Substring(2);
                                break;
                            }
                        break;

                    case "AQ":      // Air Quality
                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "CO":
                                CO2.Text = string.Format("CO2: {0}", token.Substring(2));
                                break;

                            case "TV":
                                TVOC.Text = string.Format("TVOC: {0}", token.Substring(2));
                                break;
                            }
                        break;

                    case "TP":      // Temperature
                        Temperature.Text = string.Format("Temperature: {0}", msg.Substring(2));
                        break;

                    case "HU":      // Humidity
                        Humidity.Text = string.Format("Humidity: {0}", msg.Substring(2));
                        break;

                    case "MR":      // Moisture
                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "MR":
                                pot = Utility.Parse.AtoI(token.Substring(2));
                                break;

                            case "RE":
                                LastReading[pot] = Utility.Parse.AtoI(token.Substring(2));
                                break;

                            case "PE":
                                    tmp = Utility.Parse.AtoI(token.Substring(2));
                                    Pot[pot].CurMoist.Text = string.Format("Moisture Level: {0}", (tmp >= 0) ? tmp.ToString() : "???");
                                    break;
                                }
                            break;

                    case "TH":      // Moisture threshold
                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "TH":
                                pot = Utility.Parse.AtoI(token.Substring(2));
                                break;

                            case "NM":
                                BlockUpdate = true;
                                Pot[pot].TargetMoist.Value = Utility.Parse.AtoI(token.Substring(2));
                                BlockUpdate = false;
                                break;
                            }
                        break;

                    case "WI":      // Watering interval
                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "WI":
                                pot = Utility.Parse.AtoI(token.Substring(2));
                                break;

                            case "NM":
                                BlockUpdate = true;
                                Pot[pot].WateringInterval.Value = Utility.Parse.AtoI(token.Substring(2));
                                BlockUpdate = false;
                                break;
                            }
                        break;

                    case "WD":      // Water duration
                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "WD":
                                pot = Utility.Parse.AtoI(token.Substring(2));
                                break;

                            case "NM":
                                BlockUpdate = true;
                                Pot[pot].WaterDuration.Value = Utility.Parse.AtoI(token.Substring(2));
                                BlockUpdate = false;
                                break;
                            }
                        break;

                    case "WU":      // Watering Update
                        foreach (string token in msg.Split('|'))
                            switch (token.Substring(0, 2))
                            {
                            case "WU":
                                pot = Utility.Parse.AtoI(token.Substring(2));
                                break;

                            case "NW":
                                tmp = Utility.Parse.AtoI(token.Substring(2));
                                break;

                            case "TW":
                                Pot[pot].SecsTilWater.Text = (tmp == 0) ? string.Empty : string.Format("Seconds until water: {0}", token.Substring(2));
                                break;

                            case "PO":
                                tmp = Utility.Parse.AtoI(token.Substring(2));
                                if (tmp > 0)
                                {
                                    Pot[pot].Watering.Visible = true;
                                    Pot[pot].ForceWater.Visible = false;
                                    Pot[pot].Watering.Text = string.Format("Watering {0}", tmp);
                                }
                                else
                                {
                                    if (Pot[pot].Watering.Visible && POM.Simulated && Pot[pot].SimMoisture.Value > 60)
                                        Pot[pot].SimMoisture.Value -= 50;
                                    Pot[pot].Watering.Visible = false;
                                    Pot[pot].ForceWater.Visible = true;
                                }
                                break;
                            }
                        break;
                    }
                }
                break;
            }
        }

        #endregion

        #region Tools

        private void Log(string data, Color? color = null, bool big = false)
        {
            Font currentFont = LogBox.SelectionFont;

            if (big)
                LogBox.SelectionFont = bigFont;

            LogBox.SelectionColor = color ?? Color.Black;
            LogBox.AppendText("\n" + data);
            LogBox.SelectionFont = currentFont;

            //  Auto-scroll the text box
            SuspendLayout();
            LogBox.ScrollToCaret();
            ResumeLayout(true);
        }
        private readonly Font bigFont = new Font("Arial", 14F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(0)));

        private void LogException(Exception e)
        {
            StringBuilder str = new StringBuilder();

            while (e != null)
            {
                str.Append(e.Message + "\r\n\t");
                str.Append(e.StackTrace + "\r\n\t");
                e = e.InnerException;
            }

            Log(str.ToString(), Color.Red);
        }

        #endregion

        #region UI Events

        private void CalSensor_Click(object sender, EventArgs e)
        {
            Enabled = false;

            (new CalibrateMoistureSensor()).ShowDialog();

            Enabled = true;
        }

        private void ChangeAllNames_Click(object sender, EventArgs e)
        {
            Enabled = false;

            (new ChangeNames()).ShowDialog();

            Enabled = true;
        }

        private void TargetMoisture1_ValueChanged(object sender, EventArgs e)
        {
            if (!BlockUpdate)
                POM.UpdateThreshold(0, (int)TargetMoisture1.Value);
        }

        private void TargetMoisture2_ValueChanged(object sender, EventArgs e)
        {
            if (!BlockUpdate)
                POM.UpdateThreshold(1, (int)TargetMoisture2.Value);
        }

        private void WaterDuration1_ValueChanged(object sender, EventArgs e)
        {
            if (!BlockUpdate)
                POM.UpdateWaterDuration(0, (int)WaterDuration1.Value);
        }

        private void WaterDuration2_ValueChanged(object sender, EventArgs e)
        {
            if (!BlockUpdate)
                POM.UpdateWaterDuration(1, (int)WaterDuration2.Value);
        }

        private void WateringInterval1_ValueChanged(object sender, EventArgs e)
        {
            if (!BlockUpdate)
                POM.UpdateWateringInterval(0, (int)WateringInterval1.Value);
        }

        private void WateringInterval2_ValueChanged(object sender, EventArgs e)
        {
            if (!BlockUpdate)
                POM.UpdateWateringInterval(1, (int)WateringInterval2.Value);
        }

        private void SimMoisture1_ValueChanged(object sender, EventArgs e)
        {
            if (!BlockUpdate)
                POM.UpdateSimulatedReading(0, (int)SimMoisture1.Value);
        }

        private void SimMoisture2_ValueChanged(object sender, EventArgs e)
        {
            if (!BlockUpdate)
                POM.UpdateSimulatedReading(1, (int)SimMoisture2.Value);
        }

        private void ForceWater1_Click(object sender, EventArgs e)
        {
            POM.ForceWatering(0);
            ForceWater1.Visible = false;
        }

        private void ForceWater2_Click(object sender, EventArgs e)
        {
            POM.ForceWatering(1);
            ForceWater2.Visible = false;
        }

        private void ClearLog_Click(object sender, EventArgs e)
        {
            LogBox.Text = string.Empty;
        }

        private void DatascopeEnable_CheckedChanged(object sender, EventArgs e)
        {
            Protocol.DataScope = (DatascopeEnable.Checked) ? DataScopeLevel.Packets : DataScopeLevel.None;
        }

        #endregion
    }

    #region PotDef
    public class PotDef
    {
        public PotDef(GroupBox potGroup, Label curMoist, NumericUpDown waterDuration, NumericUpDown wateringInterval,
            NumericUpDown targetMoist, Label secsTilWater, TextBox watering, Button forceWater, NumericUpDown simMoisture)
        {
            CurMoist = curMoist;
            Watering = watering;
            PotGroup = potGroup;
            ForceWater = forceWater;
            TargetMoist = targetMoist;
            SimMoisture = simMoisture;
            SecsTilWater = secsTilWater;
            WaterDuration = waterDuration;
            WateringInterval = wateringInterval;
        }

        public readonly Label CurMoist;
        public readonly TextBox Watering;
        public readonly GroupBox PotGroup;
        public readonly Button ForceWater;
        public readonly Label SecsTilWater;
        public readonly NumericUpDown SimMoisture;
        public readonly NumericUpDown TargetMoist;
        public readonly NumericUpDown WaterDuration;
        public readonly NumericUpDown WateringInterval;
    }
    #endregion
}
