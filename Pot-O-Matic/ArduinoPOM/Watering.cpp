#include "GalTam.h"
#include "Moisture.h"
#include "PotConfig.h"

extern Service *PotService[MAX_POT];

//******************************************** WaterPump Support *********************************************
//  There is one water pump for each pot

static int WaterPin[MAX_POT] = { 3, 5 };   // These match up to the pin controlling the motor for each pot

class WaterPump
{
public:
  WaterPump(int p)
      : pot(p), pumpPin(WaterPin[p]), ticksTilWater(30), dirty(true), forceWater(false), pumpOnRemaining(0), lastNeedToWater(false)
    {
    pinMode(pumpPin, OUTPUT);   // Set up the control pin for the motor
    digitalWrite(pumpPin, 0);   // Make sure it is off

    blueNeedToWater = new Characteristic(pot + 1, POT_NeedToWater, "NeedToWater", PotService[p], BLERead | BLENotify);
    blueTicksTilWater = new Characteristic(pot + 1, POT_SecsTilWater, "SecsTilWater", PotService[p], BLERead | BLENotify);
    bluePumpOnRemaining = new Characteristic(pot + 1, POT_PumpOnRemainingSeconds, "PumpOnRemainingSeconds", PotService[p], BLERead | BLENotify);
    }

  // Manually force pump on for standard pumping period
  void ForceWater()
    {
    forceWater = true;
    Log(LC_Blue, false, "Pot %d: Watered manually", pot + 1);
    }

  // A system event has occurred.  Update data accordingly
  void SystemEvent(SystemEventType type, int n)
    {
    if (type == SE_PC_Reconnect)
      dirty = true;
    }

  // Ticks once/sec.  Manage pump for one pot based on moisture sensor readings
  void Tick()
    {
    bool needToWater = MoistureSensor.Valid(pot) && MoistureSensor.Level(pot) < MoistureSensor.Threshold(pot) && pumpOnRemaining == 0;

    if (pumpOnRemaining > 0)
      {     // If pump is on, count down to zero then turn it off
      dirty = true;
      if (--pumpOnRemaining == 0)
        digitalWrite(pumpPin, 0);
      }

    if (needToWater)
      {     // Pot too dry, manage count down til start watering
      dirty = true;
      if (ticksTilWater == 0)
        ticksTilWater = Config.GetWateringInterval(pot);
      else if (--ticksTilWater == 0)
        {   // Countdown exhausted, start watering
        digitalWrite(pumpPin, 1);
        pumpOnRemaining = Config.GetWaterDuration(pot);
        Log(LC_Green, true, "Pot %d: Watered", pot + 1);
        }
      }
    else
      ticksTilWater = 0;

    if (pumpOnRemaining == 0 && forceWater)
      {     // Not currently watering and manual water requested, turn on pump
      dirty = true;
      forceWater = false;
      digitalWrite(pumpPin, 1);
      pumpOnRemaining = Config.GetWaterDuration(pot);
      }

    if (pumpOnRemaining != lastPumpRemaining)
      bluePumpOnRemaining->Write(pumpOnRemaining);
    lastPumpRemaining = pumpOnRemaining;

    if (ticksTilWater != lastTicksTilWater)
      {
      dirty = true;
      blueTicksTilWater->Write(ticksTilWater);
      }
    lastTicksTilWater = ticksTilWater;

    if (needToWater != lastNeedToWater)
      {
      dirty = true;
      blueNeedToWater->Write(needToWater);
      }
    lastNeedToWater = needToWater;

    if (HostProtocol::Communicating())
      {     // Inform host of any changes it needs to know about
      if (dirty)
        {
        char scr[50];

        dirty = false;
        sprintf(scr, "WU%d|NW%d|TW%d|PO%d", pot, needToWater, ticksTilWater, pumpOnRemaining);
        HostProtocol::Send(scr);
        }
      }
    }

private:
  int pot;
  bool dirty;
  int pumpPin;
  bool forceWater;
  int ticksTilWater;
  int pumpOnRemaining;
  bool lastNeedToWater;
  int lastPumpRemaining;
  int lastTicksTilWater;


  Characteristic *blueNeedToWater;
  Characteristic *blueWaterDuration;
  Characteristic *blueTicksTilWater;
  Characteristic *bluePumpOnRemaining;
  Characteristic *blueWateringInterval;
};

//******************************************** Watering Support *********************************************
// High level manager for all pumps

class Watering : public Ticker
{
public:
  Watering() : Ticker("Watering", 1, OE_Late), FW_Cmd("FW", FW_Func) { }

protected:
  virtual void Initialize()
    {
    for (int p = 0; p < MAX_POT; p++)
      Pump[p] = new WaterPump(p);
    }

  virtual void Tick()
    {
    for (int p = 0; p < MAX_POT; p++)
      Pump[p]->Tick();
    }

  virtual void SystemEvent(SystemEventType type, int n)
    {
    for (int p = 0; p < MAX_POT; p++)
      Pump[p]->SystemEvent(type, n);
    }

private:
  // Host commands force watering on a pump
  static Str *FW_Func(char *msg)
    {
    int p = msg[2] - '0';

    if (p >= 0 && p < MAX_POT)
      Pump[p]->ForceWater();

    return NULL;
    }

  HostCommand FW_Cmd;
  static WaterPump *Pump[];
} waterControl;

WaterPump *Watering::Pump[MAX_POT];
