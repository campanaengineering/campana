//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#include "GalTam.h"
#include "Version.h"
#include "Moisture.h"

USHORT Checksum = 0;
bool SendIdCommand = false;

//********************************* HostCommand Support ******************************************************************
List *HostCommand::list = NULL;

/// Constructor for HostCommand
HostCommand::HostCommand(
    const char *cmd,                            ///< Command type
    Str *(*cmdFunc)(char *)				        ///< Function to execute
) : Obj(), func(cmdFunc)
  {
  if (list == NULL)
    list = new List;

  list->AddFirst(this);
  command = (*cmd++ << 8) + *cmd;
  }

/// Call the correct function to process the command
void HostCommand::Execute(Str *pCmd)
  {
  char *pa = (char *)pCmd->Value();
  short type = (*pa++ << 8) + *pa;

  for (HostCommand *pDict = (HostCommand *)list->First(); pDict; pDict = (HostCommand *)pDict->Next())
    if (pDict->command == type)
      HostProtocol::Send(pDict->func((char *)pCmd->Value()));

  pCmd->Done();
  }

//********************************* HostCommandExec Support ******************************************************************

class HostCommandExec : public Ticker
  {
  public:
    HostCommandExec() : Ticker("HostCommandExec", 10), ID_Cmd("ID", ID_Func), statusResponse("HI", true) {  }

  private:
    HostCommand ID_Cmd;
    Str statusResponse;

    static bool sendIdentity;

    static Str *ID_Func(char *msg = NULL)
      {
      char id[50];
      static bool first = true;

      sendIdentity = SendIdCommand = false;
      sprintf(id, "id|VE%s|HW%s|CS%d|DN%s%s", VERSION, HARDWARE, Checksum, Config.GetDeviceName(), Moisture::Simulated() ? "|SM" : "");

      HostProtocol::Send(id);

      if (first || msg != NULL)
        SendSystemEvent(SE_PC_Reconnect);
      first = false;

      return NULL;
      }

  protected:
    virtual void Tick()
      {
      Obj *pCmd = HostProtocol::Receive();

      if (SendIdCommand)
        ID_Func();
      else if (pCmd)
        if (pCmd->GetObjType() == Type_String)
          HostCommand::Execute((Str *)pCmd);        // Command received, process it
        else if (sendIdentity)
          ID_Func();
        else if (!statusResponse.InSomeList())
          HostProtocol::Send(&statusResponse);
      }
  } Commander;

  bool HostCommandExec::sendIdentity = true;
