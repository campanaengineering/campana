﻿#region Using

using System;
using System.Threading;
using System.Collections.Generic;

using Utility;

#endregion

namespace Pot_O_Matic_Interface
{
    #region Enumerations

    /// <summary>
    /// Executive states
    /// </summary>
    public enum ExecState
    {
        Unknown,
        Down,
        ReadyForCommand,

        Initializing,
        Downloading,
    }

    /// <summary>
    /// Last state of communication reported
    /// </summary>
    public enum LastComReport
    {
        Unknown,
        ReportedUp,
        ReportedDown,
    }

    #endregion

    /// <summary>
    /// Executive for controlling the Funnel
    /// </summary>
    public class Executive : IDisposable
    {
        #region Variables

        /// <summary>
        /// Current state of Executive
        /// </summary>
        public static ExecState State { get; private set; } = ExecState.Unknown;

        /// <summary>
        /// True if error opening serial port
        /// </summary>
        public bool PortOpenFailure { get; private set; }

        /// <summary>
        /// One second in milliseconds
        /// </summary>
        private const int OneSecond = 1000;

        /// <summary>
        /// The protocol handler
        /// </summary>
        public Protocol protocol = null;

        /// <summary>
        /// The message handler
        /// </summary>
        private string _message = null;

        /// <summary>
        /// True when too many download attempts have failed
        /// </summary>
        private int _downloadRetries = 0;
        private const int MaxDownloadRetriesAllowed = 3;

        /// <summary>
        /// When ArduinoIO went down
        /// </summary>
        private DateTime wentDown = DateTime.Now;

        /// <summary>
        /// Is the Funnel device driver running
        /// </summary>
        public static bool Running { get; private set; }

        /// <summary>
        /// The Executive thread
        /// </summary>
        private static Thread _executiveThread = null;

        /// <summary>
        /// Set true after successful download to cause version information update
        /// </summary>
        private static bool updateVersionID = false;

        /// <summary>
        /// Last communication report type
        /// </summary>
        private LastComReport _lastComReport = LastComReport.Unknown;

        /// <summary>
        /// Current protocol application state
        /// </summary>
        private AppStateType _protAppState = AppStateType.Unknown;

        /// <summary>
        /// Protocol delegate
        /// </summary>
        private Protocol.ProtocolEventDelegate _protDelegate;

        /// <summary>
        /// Event triggered when app state changes
        /// </summary>
        private AutoResetEvent _appStateChanged = new AutoResetEvent(false);

        /// <summary>
        /// Event triggered when message thread to be terminated
        /// </summary>
        private AutoResetEvent _terminateMessageThread = new AutoResetEvent(false);

        /// <summary>
        /// Event triggered when Executive thread to be terminated
        /// </summary>
        private AutoResetEvent _terminateExecutiveThread = new AutoResetEvent(false);

        /// <summary>
        /// Tx Message queue
        /// </summary>
        private MessageQueue<string> _TxMessageQueue = new MessageQueue<string>();

        /// <summary>
        /// Rx message queue
        /// </summary>
        private MessageQueue<string> _RxMessageQueue = new MessageQueue<string>(50);

        /// <summary>
        /// Current connection to Data Funnel
        /// </summary>
        public bool Connected { get; private set; }

        /// <summary>
        /// Object that watches for changes in Funnel system files
        /// </summary>
        public static FileWatcher FileWatch { get; private set; }

        /// <summary>
        /// Current instance of the Executive (there should only be one)
        /// </summary>
        public static Executive Instance { get; private set; }

        #endregion

        #region Startup/Shutdown

        /// <summary>
        /// Create new Executive
        /// </summary>
        public Executive()
        {
            Running = true;
            Instance = this;
            Connected = true;
            PortOpenFailure = false;
            State = ExecState.Unknown;

            ThreadPool.QueueUserWorkItem(new WaitCallback(MessageThread));
            ThreadPool.QueueUserWorkItem(new WaitCallback(ExecutiveThread));
        }

        /// <summary>
        /// The destructor
        /// </summary>
        ~Executive()
        {
            Dispose();
        }

        /// <summary>
        /// Dispose of object
        /// </summary>
        public void Dispose()
        {
            Instance = null;
            Running = false;
            _terminateExecutiveThread.Set();
        }

        #endregion

        #region Events

        /// <summary>
        /// Received a protocol event
        /// </summary>
        /// <param name="type">Evnt type</param>
        /// <param name="msg">Optional text</param>
        private void Exec_ProtocolEvent(ProtocolEventType type, string msg)
        {
            switch (type)
            {
                case ProtocolEventType.Log:
                    Log(msg);
                    break;

                case ProtocolEventType.DataScope:
                    SendArduinoEvent(ArduinoEventType.DataScope, msg);
                    break;

                case ProtocolEventType.AppStateChanged:
                    _appStateChanged.Set();
                    break;

                case ProtocolEventType.MessageReceived:
                    _RxMessageQueue.Enqueue(msg);
                    break;
            }
        }

        /// <summary>
        /// File watcher reported file has changed
        /// </summary>
        /// <param name="logMess">If not null is message to log</param>
        void FileWatcher_FileChangedEvent(string logMess)
        {
            if (logMess != null)
                Log(logMess, "blue");
        }

        /// <summary>
        /// Send an Arduino Event
        /// </summary>
        /// <param name="ev">Event type</param>
        /// <param name="text">Optional text</param>
        /// <param name="obj">Optional object</param>
        private void SendArduinoEvent(ArduinoEventType ev, Object obj = null)
        {
            if (Instance != null)
                ArduinoCom.Instance.SendEvent(ev, obj);
        }

        /// <summary>
        /// Log some text
        /// </summary>
        /// <param name="text">Text to log</param>
        public void Log(string text, string color = null, bool big = false)
        {
            ArduinoCom.Instance.SendEvent(ArduinoEventType.StatusUpdate,
                string.Format("LG|CO{0}{1}|TX{2}", (color == null) ? "black" : color, big ? "|BG" : string.Empty, text));
        }

        #endregion

        #region Tools

        /// <summary>
        /// Pause protocol during which COM port is available
        /// </summary>
        public static void PauseProtocol()
        {
            if (Instance != null && Instance.protocol != null)
                Instance.protocol.PauseProtocol();
        }

        /// <summary>
        /// Restart protocol after being paused
        /// </summary>
        public static void RestartProtocol()
        {
            if (Instance != null && Instance.protocol != null)
                Instance.protocol.RestartProtocol();
        }

        /// <summary>
        /// Log exception data
        /// </summary>
        /// <param name="e">The exception</param>
        private void LogException(Exception e)
        {
            while (e != null)
            {
                Log(e.Message, "red", true);
                Log(e.StackTrace, "red");
                e = e.InnerException;
            }
        }

        /// <summary>
        /// Log an exception
        /// </summary>
        /// <param name="e">The execption</param>
        internal static void InternalLogException(Exception e)
        {
            Instance.LogException(e);
        }

        /// <summary>
        /// True if communicating
        /// </summary>
        public bool Communicating { get { return (protocol != null && protocol.AppState >= AppStateType.AppReady); } }

        /// <summary>
        /// String to used to represent the firmware version
        /// </summary>
        public static string VersionID
        {
            get { return string.Format("{0}.{1}", FileWatch.FileTime, ArduinoCom.Instance.Checksum); }
        }

        public static string LastVersionID
        {
            get { return _cvid; }
            set { _cvid = value; Parse.SetPersistedVariable("Firmware", "ArduinoVersionID", value); }
        }
        private static string _cvid = Parse.GetPersistedVariable("Firmware", "ArduinoVersionID", "0.0");

        #endregion

        #region State Machine

        /// <summary>
        /// State machine
        /// </summary>
        /// <returns>True if another state needs processing immediately</returns>
        private bool ProcessExecutiveState()
        {
            switch (State)
            {
            case ExecState.ReadyForCommand:
                if (_lastComReport != LastComReport.ReportedUp)
                {
                    _lastComReport = LastComReport.ReportedUp;
                }
                if (FileWatch.DownloadName != string.Empty
                    && ArduinoCom.Instance.Version != string.Empty
                    && VersionID != LastVersionID
                    && _downloadRetries < MaxDownloadRetriesAllowed)
                {
                    State = ExecState.Downloading;
                    return true;
                }
                break;

            case ExecState.Down:
                if (_lastComReport != LastComReport.ReportedDown)
                {
                    identifyDevices = true;
                    _lastComReport = LastComReport.ReportedDown;
                    ArduinoCom.Instance.SendEvent(ArduinoEventType.ArduinoConnected, false);
                }
                break;

            case ExecState.Initializing:
                if (_protAppState == AppStateType.AppReady)
                    State = ExecState.ReadyForCommand;
                return true;

            case ExecState.Downloading:
                protocol.PauseProtocol();
                Thread.Sleep(1000);

                if (FirmwareDownload.Start(FileWatch.DownloadName, Protocol.ComPort))
                {
                    updateVersionID = true;
                    Log("Pot-O-Matic firmware updated successfully", "blue", true);
                }
                else
                    Log("Failure executing ArduinoIO loader program", "red", true);
                _downloadRetries++;
                identifyDevices = true;
                State = ExecState.Down;
                protocol.RestartProtocol();
                ArduinoCom.Instance.Version = string.Empty;
                break;
            }

            return false;
        }

        #endregion

        #region Messages

        /// <summary>
        /// Send a message
        /// </summary>
        /// <param name="msg">Text of message.</param>
        public void SendCommand(string msg, bool waitSent = false)
        {
            _TxMessageQueue.Enqueue(msg);
            if (waitSent)
                for (int i = 0; i < 50 && !protocol.TxEmpty; i++)
                    Thread.Sleep(OneSecond / 10);
        }

        private void ProcessResponse(string msg)
        {
            switch (msg.Substring(0,2))
            {
            case "HI":      // Indicates device alive
                if (identifyDevices)
                    SendCommand("ID");
                break;

            case "id":      // Identification response
                {
                foreach (string token in msg.Split('|'))
                    switch (token.Substring(0,2))
                    {
                    case "VE":
                        ArduinoCom.Instance.Version = token.Substring(2);
                        break;

                    case "HW":
                        ArduinoCom.Instance.Hardware = token.Substring(2);
                        break;

                    case "CS":
                        ArduinoCom.Instance.Checksum = Parse.AtoI(token.Substring(2));
                        if (updateVersionID)
                            LastVersionID = VersionID;
                        updateVersionID = false;
                        break;

                    case "DN":
                        ArduinoCom.Instance.DeviceName = token.Substring(2);
                        break;

                    case "SM":
                        ArduinoCom.Instance.Simulated = true;
                        break;
                    }
                ArduinoCom.Instance.SendEvent(ArduinoEventType.ArduinoConnected, true);
                identifyDevices = false;
                }
                break;

            default:            // Everything else
                ArduinoCom.Instance.SendEvent(ArduinoEventType.StatusUpdate, msg);
                break;
            }
        }
        private bool identifyDevices = true;

        /// <summary>
        /// The message thread
        /// </summary>
        /// <param name="ignored">Thread variable is unused</param>
        private void MessageThread(Object ignored)
        {
            WaitHandle[] waitList = new WaitHandle[2];  // Events to be monitored and acted upon

            waitList[0] = _TxMessageQueue.Sem;            // Message queue for Funnel
            waitList[1] = _terminateMessageThread;      // End thread when set

            while (Running)
                try
                {
                    switch (WaitHandle.WaitAny(waitList))
                    {
                    case 0:                                     // Send this message to the Funnel
                        _message = _TxMessageQueue.Dequeue();

                        if (protocol.AppState == AppStateType.AppReady)
                            protocol.SendMessage(_message);
                        _message = null;
                        break;

                    case 1:                                     // End message thread
                        // _running will be false if get here
                        break;
                    }
                }
                catch { }

            if (_protDelegate != null)
                protocol.ProtocolEvent -= _protDelegate;
            if (protocol != null)
                protocol.Dispose();
            protocol = null;

            FileWatch = null;
        }

        #endregion

        #region Executive Thread

        /// <summary>
        /// The Executive thread
        /// </summary>
        private void ExecutiveThread(Object ignored)
        {
            Queue<string> queue;
            WaitHandle[] waitList = new WaitHandle[3];                        // Events to be monitored and acted upon (except commands)

            _executiveThread = Thread.CurrentThread;

            try
            {
                protocol = new Protocol();
                _protDelegate = new Protocol.ProtocolEventDelegate(Exec_ProtocolEvent);
                protocol.ProtocolEvent += _protDelegate;
                protocol.Start();
                Connected = false;
            }
            catch (Exception)
            {
                Running = false;
                PortOpenFailure = true;
                _terminateMessageThread.Set();
            }

            FileWatch = new FileWatcher();
            FileWatch.FileChangedEvent += new FileWatcher.FileChangedEventDelegate(FileWatcher_FileChangedEvent);

            waitList[0] = _appStateChanged;             // Protocol app state has changed
            waitList[1] = _RxMessageQueue.Sem;          // Rx message
            waitList[2] = _terminateExecutiveThread;    // End thread when set

            while (Running)
                try
                {
                    do
                    {
                        queue = _RxMessageQueue.Contents;     // Process Rx messages occurring during state processing
                        if (queue != null)
                        {
                            while (queue != null && queue.Count != 0)
                                ProcessResponse(queue.Dequeue());
                            waitList[1] = _RxMessageQueue.Sem;
                        }

                        if (protocol.AppState != _protAppState)
                        {       // Protocol state has changed, take whatever steps necessary
                            _protAppState = protocol.AppState;

                            switch (_protAppState)
                            {
                            case AppStateType.Down:
                                identifyDevices = true;
                                State = ExecState.Down;
                                wentDown = DateTime.Now;
                                break;

                            case AppStateType.AppReady:
                                FileWatch.UpdateFileInfo();
                                State = ExecState.Initializing;
                                break;
                            }
                        }
                    }
                    while (Running && ProcessExecutiveState());

                    switch (WaitHandle.WaitAny(waitList, OneSecond * 2))
                    {
                    case 0:                                     // AppState changed
                        break;

                    case 1:                                     // Rx message
                        ProcessResponse(_RxMessageQueue.Dequeue());
                        break;

                    case 2:                                     // End executive
                        Running = false;
                        break;

                    case WaitHandle.WaitTimeout:
                        break;
                    }
                }
                catch { }

            try
            {
                if (FileWatch != null)
                    FileWatch.Dispose();

                protocol.ProtocolEvent -= _protDelegate;
                _terminateMessageThread.Set();
            }
            catch { }
        }

        #endregion
    }
}
