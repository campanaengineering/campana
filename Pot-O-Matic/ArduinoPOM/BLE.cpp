//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#include "GalTam.h"
#include "Version.h"

#define CREATE_SYSTEM               -1234

bool BlueToothConnected = false;
bool BlueToothOperational = false;

// Create a 16-bit UUID
const char *ToUUID(int uuid)
  {
  char *charUUID = new char[5];
  StrOp::itoh(uuid, charUUID, 4);
  return charUUID;
  }

//********************************* Service Support ******************************************************************

List *Service::list = NULL;
Service *Service::systemService = NULL;
HostCommand Service::DN_Cmd("DN", DN_Func);
Characteristic *Service::blueDeviceName = NULL;
int Service::nextUUID = ADDITIONAL_SERVICE_UUID_BASE;

void Service::FinishConstruction(const char *type, int index, Service *toAdd)
  {
  if (list == NULL)
    {
    list = new List;
    BlueToothOperational = BLE.begin();
    systemService = new Service("System", SYSTEM_UUID);
    new Characteristic(0, SSC_Version, "Version", systemService, BLERead, VERSION);
    new Characteristic(0, SSC_Hardware, "Hardware", systemService, BLERead, HARDWARE);
    new Characteristic(0, SSC_Checksum, "Checksum", systemService, BLERead, Checksum);
    blueDeviceName = new Characteristic(0, SSC_DeviceName, "DeviceName", systemService, BLERead | BLEWrite | BLENotify, Config.GetDeviceName(), BlueUpdatedDeviceName);
    }

  if (toAdd)
    {
    list->AddLast(toAdd);
    new Characteristic(index, SC_Type, "$Type", toAdd, BLERead, type);
    new Characteristic(index, SC_Index, "$Index", toAdd, BLERead, index);
    }
  }

void Service::FinishInitialization()
  {                 // Services need to be added to device after all characteristics have been defined and added to services
  if (BlueToothOperational && list)
    {
    for (Service *pDict = (Service *)list->First(); pDict; pDict = (Service *)pDict->Next())
      {
      BLE.addService(pDict->bleService);
      if (pDict->advertised)
        BLE.setAdvertisedService(pDict->bleService);
      }

    BLE.setLocalName(Config.GetDeviceName());
    BLE.setDeviceName(Config.GetDeviceName());
    BLE.advertise();
    }
  }

void Service::SetDeviceName(const char *name)
  {
  SendIdCommand = true;
  name = Config.SetDeviceName(name);
  if (blueDeviceName != NULL)
    blueDeviceName->Write(name);

  if (BlueToothOperational)
    {
    BLE.setLocalName(name);
    BLE.setDeviceName(name);
    }
  }

Service *Service::GetSystemService()
  {
  if (systemService == NULL)
    FinishConstruction(NULL, -1, NULL);

  return systemService;
  }

//********************************* Characteristic Support ******************************************************************

List *Characteristic::list = NULL;
int Characteristic::nextDescriptorIndex = 0;

// Constructor for numeric value
Characteristic::Characteristic(int index, int uuid, const char *name, Service *serv, uint8_t properties, int initValue, void (*func)(void *, const char *), void *obj)
      : Obj(), setFunc(func), setObj(obj)
  {
  StrOp::IntToString((char *)val, initValue);
  FinishConstruction(index, uuid, name, serv, properties);
  }

// Constructor for string value
Characteristic::Characteristic(int index, int uuid, const char *name, Service *serv, uint8_t properties, const char *initValue, void (*func)(void *, const char *), void *obj)
      : Obj(), setFunc(func), setObj(obj)
  {
  StrOp::Copy((char *)val, (initValue) ? initValue : "0", sizeof(val));
  FinishConstruction(index, uuid, name, serv, properties);
  }

void Characteristic::FinishConstruction(int index, int uuid, const char *name, Service *serv, uint8_t properties)
  {
  if (list == NULL)
    list = new List;

  list->AddLast(this);

  if (serv == NULL)
    serv = Service::GetSystemService();

  if (BlueToothOperational)
    {
    bleCharacteristic = new BLECharacteristic(ToUUID((index * 0x1000) + uuid), properties, sizeof(val));

            // Add descriptor with name of variable
    AddDescriptor(name);

    bleCharacteristic->writeValue(val, strlen((const char *)val) + 1);

    if (setFunc != NULL)
      bleCharacteristic->setEventHandler(BLEWritten, written);

    serv->AddCharacteristic(*bleCharacteristic);
    }
  }

int Characteristic::AddDescriptor(const char *contents)
  {
  if (BlueToothOperational)
    {
    uint8_t *scr = new uint8_t[strlen(contents) + 1];

    StrOp::Copy((char *)scr, contents);
    bleCharacteristic->addDescriptor(*(new BLEDescriptor(ToUUID(DESCRIPTOR_UUID_BASE + nextDescriptorIndex), scr, strlen((const char *)scr) + 1)));

    return nextDescriptorIndex++;
    }
  return -1;
  }

void Characteristic::Write(const char *src)
  {
  int len = StrOp::Copy((char *)val, src);
  if (BlueToothOperational)
    bleCharacteristic->writeValue(val, len + 1);
  }

void Characteristic::Write(int num)
  {
  char scr[10];
  StrOp::IntToString(scr, num);
  Write(scr);
  }

void Characteristic::written(BLEDevice central, BLECharacteristic characteristic)
  {
  if (list)
    for (Characteristic *pDict = (Characteristic *)list->First(); pDict; pDict = (Characteristic *)pDict->Next())
      if (strcmp(characteristic.uuid(), pDict->bleCharacteristic->uuid()) == 0)
        {
        uint8_t scr[CHAR_VAL_MAX_WIDTH];

        StrOp::Clear((char *)scr, sizeof(scr));
        characteristic.readValue(scr, sizeof(scr));
        pDict->setFunc(pDict->setObj, (const char *)scr);
        break;
        }
  }

//********************************* Putting it all together ******************************************************************

class BlueTooth : public Ticker
{
public:
  BlueTooth() : Ticker("BlueTooth", -1, OE_Last) { }

protected:
  virtual void Initialize()
    {
    Service::FinishInitialization();
    }

  virtual void Poll()
    {
    if (BlueToothOperational)
      {
      BLE.poll();
      BlueToothConnected = BLE.connected();
      }
    }
} ble;
