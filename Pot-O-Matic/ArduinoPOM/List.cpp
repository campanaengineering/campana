//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#include "GalTam.h"

/***************************************** Up Support ***************************************/

Up::Up(const char *format, ...)
  {
  *text = '\0';
  if (format)
    {
    va_list args;

    va_start(args, format);
    vsprintf(text, format, args);
    va_end(args);
    }
  }

/***************************************** Integer Support ***************************************/

List Integer::freeList;

Integer *Integer::Alloc(int val)
  {
  Integer *instance = (Integer *)freeList.TakeFirst();

  if (instance)
    instance->value = val;
  else
    instance = new Integer(val);

  return instance;
  }

void Integer::Done()
  {
  if (freeList.Count() < 5)
    freeList.AddFirst(this);
  else
    delete this;
  }

/***************************************** Str Support ***************************************/

List Str::freeList;

Str::Str(const char *val, bool keep) : Obj(), keepAround(keep)
  {
  if (strlen(val) <= LOCAL_STR_SIZE)
    {
    strcpy(local, val);
    value = local;
    }
  else
    value = StrOp::replace(val);
  }

Str *Str::Alloc(const char *val, bool keep)
  {
  Str *instance = (Str *)freeList.TakeFirst();

  if (instance)
    {
    instance->keepAround = keep;
    if (strlen(val) <= LOCAL_STR_SIZE)
      {
      strcpy(instance->local, val);
      instance->value = instance->local;
      }
    else
      instance->value = StrOp::replace(val);
    }
  else
    instance = new Str(val, keep);

  return instance;
  }

void Str::Done()
  {
  if (!keepAround)
    if (freeList.Count() >= 50)
      delete this;
    else
      {
      Free();
      value = NULL;
      freeList.AddFirst(this);
      }
  }

/***************************************** List Support ***************************************/

/// Remove last item from list and return it
/// \return The item
Obj *List::TakeLast()
  {
  Obj *pItem, *pLast;

  pItem = head;

  if (pItem)
    {
    for (pLast = NULL; pItem->next; pLast = pItem, pItem = pItem->next)
      ;

    if (pLast)
      pLast->next = NULL;
    else
      head = NULL;

    pItem->next = NULL;
    pItem->inSomeList = false;
    }

  return pItem;
  }

/// Remove first item from list and return it
/// \return The item
Obj *List::TakeFirst()
  {
  Obj *pItem = head;

  if (pItem)
    {
    head = pItem->next;
    pItem->next = NULL;
    pItem->inSomeList = false;
    }

  return pItem;
  }

/// Remove passed item from the list
/// \return The item
Obj *List::Take(
    Obj *pTake						///< Item to remove
)
  {
  Obj *pItem, *pLast;

  pItem = head;

  if (pTake && pTake->inSomeList && head)
    for (pLast = NULL; pItem; pLast = pItem, pItem = pItem->next)
      if (pItem == pTake)
        {
        if (pLast)
          pLast->next = pItem->next;
        else
          head = pItem->next;

        pItem->next = NULL;
        pItem->inSomeList = false;
        break;
        }

  return (pItem == pTake) ? pItem : NULL;
  }

/// See if requested item is in the list
/// \return True if in list
bool List::InList(
    Obj *pFind						///< Item to see if present
)
  {
  Obj *pItem;
  bool found = false;

  if (head && pFind && pFind->inSomeList)
    for (pItem = head; pItem; pItem = pItem->next)
      if (pItem == pFind)
        {
        found = true;
        break;
        }

  return found;
  }

/// Count items in list
/// \return Count of items in list
int List::Count()
  {
  int count = 0;

  for (Obj *pItem = head; pItem; pItem = pItem->next)
    count++;

  return count;
  }

/// Add item to end of list
/// \return The item
Obj *List::AddLast(
    Obj *pItem							///< Item to add
)
  {
  Obj *pList;

  if (pItem)
    {
    if (pItem->inSomeList)
      {
      Serial.println("Error: Already in list");
      return NULL;
      }
    pItem->inSomeList = true;

    pItem->next = NULL;

    if (head == NULL)
      head = pItem;
    else
      {
      for (pList = head; pList->next; pList = pList->next)
        ;

      pList->next = pItem;
      }
    }

  return pItem;
  }

/// Add item to beginning of list
/// \return The item
Obj *List::AddFirst(
    Obj *pItem							///< Item to add
)
  {
  if (pItem)
    {
    if (pItem->inSomeList)
      {
      Serial.println("Error: Already in list");
      return NULL;
      }
    pItem->inSomeList = true;

    pItem->next = head;
    head = pItem;
    }

  return pItem;
  }

/// Add item to list using insertion sort based on "InsertHere"
/// \return The item
Obj *List::AddInOrder(
    Obj *pItem						///< Item to add
)
  {
  Obj *pLast, *pList;

  if (pItem)
    {
    if (pItem->inSomeList)
      {
      Serial.println("Error: Already in list");
      return NULL;
      }
    pItem->inSomeList = true;

    // Perform insertion sort of new item into list
    for (pList = head, pLast = NULL; pList; pLast = pList, pList = pList->next)
      if (pItem->InsertHere(pList))
        break;

    if (pLast)
      {
      pItem->next = pLast->next;
      pLast->next = pItem;
      }
    else
      {
      pItem->next = head;
      head = pItem;
      }
    }

  return pItem;
  }

/// Add Item to queue.
/// \return The Item
Obj *Queue::Enqueue(Obj *pItem)
  {
  Add(pItem);

  return pItem;
  }

/// Get next item, return NULL if none present.
/// \return Next item.
Obj *Queue::Dequeue()
  {
  return TakeFirst();
  }

