﻿#region Using

using System;
using System.Text;
using System.Threading;
using System.Collections.Generic;

#endregion

namespace Pot_O_Matic_Interface
{
    #region FunnelEvent

    /// <summary>
    /// Container for an Funnel Event
    /// </summary>
    public class ArduinoyEvent
    {
        /// <summary>
        /// The event type
        /// </summary>
        public int Event { get; private set; }

        /// <summary>
        /// Any associated text
        /// </summary>
        public string Text { get; private set; }

        /// <summary>
        /// Any associated generic object
        /// </summary>
        public Object Obj { get; private set; }

        /// <summary>
        /// Create a Arduinoy Event
        /// </summary>
        /// <param name="ev">Event type</param>
        /// <param name="text">Any associated text</param>
        /// <param name="obj">Any associated generic object</param>
        internal ArduinoyEvent(ArduinoEventType ev, string text = null, Object obj = null)
        {
            Obj = obj;
            Text = text;
            Event = (int)ev;
        }

        /// <summary>
        /// Create a Funnel Event
        /// </summary>
        /// <param name="ev">Event type</param>
        /// <param name="text">Any associated text</param>
        /// <param name="obj">Any associated generic object</param>
        internal ArduinoyEvent(int ev, string text = null, Object obj = null)
        {
            Obj = obj;
            Event = ev;
            Text = text;
        }
    }

    #endregion

    #region FileInfo

    /// <summary>
    /// Keeps track of version information for a file type
    /// </summary>
    public class FileInfo
    {
        /// <summary>
        /// File name
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Last modification time
        /// </summary>
        public long ModTime { get; private set; }

        /// <summary>
        /// Craete file info
        /// </summary>
        /// <param name="name">Name of file</param>
        /// <param name="modTime">File last modification time</param>
        public FileInfo(string name, long modTime)
        {
            Name = name;
            ModTime = modTime;
        }

        /// <summary>
        /// Report when a file has been downloaded
        /// </summary>
        public void FileDownloadedSuccessfully()
        {
        }
    }

    #endregion
}
