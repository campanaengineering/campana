//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#pragma once

class Moisture : public Ticker
{
public:
  Moisture() : Ticker("Moisture", 5), MP_Cmd("MP", MP_Func), MV_Cmd("MV", MV_Func) { }

  static bool Simulated();

  int Level(int p);
  bool Valid(int p);
  int Threshold(int p);
  Service& ServiceRef(int p);

private:
  virtual void Tick();
  virtual void Initialize();

  HostCommand MP_Cmd;
  static Str *MP_Func(char *msg);

  HostCommand MV_Cmd;
  static Str *MV_Func(char *msg);
};

extern Moisture MoistureSensor;
