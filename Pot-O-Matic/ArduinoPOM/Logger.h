//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#ifndef LOGGER_H
#define LOGGER_H

#define Log				Logger::LogLine

enum LogColor
  {
  LC_Black = 0,
  LC_Green,
  LC_Orange,
  LC_Red,
  LC_Blue
  };

//*********************************** Logger ******************************************

class Logger : public Ticker
{
public:
  Logger() : Ticker("Logger", 4), LV_Cmd("LV", LV_Func) { }
  static bool LogLine(LogColor lc, bool big, const char *format, ...);

private:
  static Queue LogQ;                                         ///< Where to store log messages
  static int pending;                                        ///< Items waiting to be sent

  HostCommand LV_Cmd;
  static Str *LV_Func(char *msg);

protected:
  virtual void Tick();
};

extern bool VerboseLogging;

#endif
