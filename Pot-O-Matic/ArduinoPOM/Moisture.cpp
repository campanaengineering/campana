#include "Wire.h"
#include "GalTam.h"
#include "Conf.h"
#include "Moisture.h"
#include "PotConfig.h"

extern Service *PotService[MAX_POT];

//******************************************** Moving Average Support *********************************************

#define AVERAGE_SIZE    30

class MovingAverage
{
public:
  MovingAverage() { Clear(); }

  int Add(int value) { entries++; entry[index++ % AVERAGE_SIZE] = value; return Avg(); }
  void Clear() { entries = index = 0; for (int i = 0; i < AVERAGE_SIZE; i++) entry[i] = -1; }
  int Avg()
    {
    int accum = 0, avgCount = 0;
    for (int i = 0; i < AVERAGE_SIZE; i++)
      if (entry[i] >= 0)
        avgCount++, accum += entry[i];
    return avgCount ? (((accum * 10) / avgCount) + 5) / 10 : 0;
    }

private:
  int entries;
  ULONG index;
  int entry[AVERAGE_SIZE];
};

//******************************************** MoistSense Support *********************************************

class MoistSense
{
public:
  MoistSense(int p) : pot(p), gotSensor(false), lastReading(-1)
    {
    blueReading = new Characteristic(pot + 1, POT_MoistureReading, "Moisture", PotService[p], BLERead | BLENotify);
    blueReadingPct = new Characteristic(pot + 1, POT_MoistureReadingPct, "MoisturePct", PotService[p], BLERead | BLENotify);
    }

  int Level() { return last_reported_percent; }
  int Threshold() { return Config.GetMoistureThreshold(pot); }

  virtual void Update();
  virtual bool Valid() { return false; }
  virtual bool Simulated() { return false; }

  int pot;
  bool lastCom;
  bool gotSensor;
  int lastReading;
  int last_reported_reading;
  int last_reported_percent;
  MovingAverage readingAverage;
  MovingAverage percentAverage;

  Characteristic *blueReading;
  Characteristic *blueReadingPct;
};

void MoistSense::Update()
  {
  bool updateReading = false;
  int lastReadingAvg = 0, lastPercentAvg = 0;

  lastReadingAvg = readingAverage.Add(lastReading);
  if (Config.GetMoistureWetReading(pot))
    lastPercentAvg = percentAverage.Add(map(lastReadingAvg, Config.GetMoistureWetReading(pot), Config.GetMoistureDryReading(pot), 100, 0));

  if (abs(lastReadingAvg - last_reported_reading) >= 3)
    {
    updateReading = true;
    last_reported_reading = lastReadingAvg;
    }

  if (updateReading)
    blueReading->Write(lastReadingAvg);
  if (lastPercentAvg != last_reported_percent)
    blueReadingPct->Write(lastPercentAvg);

  if (HostProtocol::Communicating())
    {
    if (updateReading || lastPercentAvg != last_reported_percent || !lastCom)
      {
      char scr[50];

      sprintf(scr, "MR%d|RE%d|PE%d", pot, lastReadingAvg, lastPercentAvg);
      HostProtocol::Send(scr);
      }
    }
  last_reported_percent = lastPercentAvg;
  lastCom = HostProtocol::Communicating();
  }

//******************************************** MoistSenseAnalog Support *********************************************

#define READ_INTERVAL   20

class MoistSenseAnalog : public MoistSense
{
public:
  MoistSenseAnalog(int p) : MoistSense(p), inputPin(p)
    {
    analogReadResolution(10);
    analogWriteResolution(10);

    if (Config.GetMoistureDryReading(pot) == 0)
      Config.SetMoistureDryReading(pot, 1023);
    }

  virtual void Update();
  virtual bool Valid() { return Config.GetMoistureWetReading(pot) != 0; }

private:
  int inputPin;
};

void MoistSenseAnalog::Update()
  {
  switch (inputPin)
    {
    case 0:
      lastReading = analogRead(A0);
      break;
    
    case 1:
      lastReading = analogRead(A1);
      break;
    }
  //Serial.printf("A%d = %d\n", inputPin, lastReading);
  MoistSense::Update();
  }

//******************************************** MoistSenseQwiic Support *********************************************

#define MOISTURE_GET_VALUE    0x05
#define MOISTURE_ADDRESS      0x28

class MoistSenseQwiic : public MoistSense
{
public:
  MoistSenseQwiic(int p) : MoistSense(p) { if (Config.GetMoistureThreshold(pot) == 0) Config.SetMoistureThreshold(p, 500); }

  bool SensorPresent();

  virtual void Update();
  virtual bool Valid() { return (gotSensor) ? lastReading >= 0 && lastReading < 1000 : false; }
};

bool MoistSenseQwiic::SensorPresent()
  {
  bool rc = false;

  if (pot == 0)
    {
    Wire.beginTransmission(MOISTURE_ADDRESS);
    rc = (Wire.endTransmission() == 0);
    }

  return gotSensor = rc;
  }

void MoistSenseQwiic::Update()
  {
  if (SensorPresent())
    {
    Wire.beginTransmission(MOISTURE_ADDRESS);
    Wire.write(MOISTURE_GET_VALUE);
    Wire.endTransmission();
    Wire.requestFrom(MOISTURE_ADDRESS, 2);    // request 1 bytes from slave device qwiicAddress

    while (Wire.available())
      lastReading = Wire.read() + (Wire.read() << 8);
    if (!Valid())
      lastReading = -1;

    Wire.read();
    }
  else
    lastReading = -1;

  MoistSense::Update();
  }

//******************************************** MoistSenseSim Support *********************************************

class MoistSenseSim : public MoistSense
{
public:
  MoistSenseSim(int p) : MoistSense(p)  { if (Config.GetMoistureThreshold(pot) == 0) Config.SetMoistureThreshold(p, 500); }

  virtual void Update();
  virtual bool Valid() { return true; }
  virtual bool Simulated() { return true; }
};

void MoistSenseSim::Update()
  {
  MoistSense::Update();
  }

//******************************************** Moisture Support *********************************************

#define MOISTURE_SENSOR_TYPE    MoistSenseAnalog

Moisture MoistureSensor;
static MoistSense *Sensor[MAX_POT];

bool Moisture::Simulated()
  {
  return Sensor[0]->Simulated();
  }

void Moisture::Initialize()
  {
  for (int p = 0; p < MAX_POT; p++)
    Sensor[p] = new MOISTURE_SENSOR_TYPE(p);
  }

void Moisture::Tick()
  {
  for (int p = 0; p < MAX_POT; p++)
    Sensor[p]->Update();
  }

int Moisture::Level(int p)
  {
  return Sensor[p]->Level();
  }

bool Moisture::Valid(int p)
  {
  return Sensor[p]->Valid();
  }

int Moisture::Threshold(int p)
  {
  return Sensor[p]->Threshold();
  }

Str *Moisture::MV_Func(char *msg)
  {
  short subType;
  short p = -1;

  for (char *pn = StrOp::NextSubCommand(msg, subType); msg; msg = pn, pn = StrOp::NextSubCommand(msg, subType))
    switch (subType)
      {
      case TwoChar('M', 'V'):           // Which moisture channel
        p = atoi(msg);
        break;

      case TwoChar('R', 'E'):           // The new sensor reading
        if (p >= 0 && p < MAX_POT)
          {
          Sensor[p]->lastReading = atoi(msg);
          Sensor[p]->Update();
          }
        break;
      }
  return NULL;
  }

Str *Moisture::MP_Func(char *msg)
  {
  short subType;
  short p = -1, wet = -1, dry = -1;

  for (char *pn = StrOp::NextSubCommand(msg, subType); msg; msg = pn, pn = StrOp::NextSubCommand(msg, subType))
    switch (subType)
      {
      case TwoChar('M', 'P'):           // Which moisture channel
        p = atoi(msg);
        break;

      case TwoChar('W', 'T'):           // The new wet reading
        if (p >= 0 && p < MAX_POT)
          wet = atoi(msg);
        break;

      case TwoChar('D', 'Y'):           // The new dry reading
        if (p >= 0 && p < MAX_POT)
          dry = atoi(msg);
        break;
      }

  if (p >= 0)
    {
    if (wet > 0)
      Config.SetMoistureWetReading(p, wet);
    if (dry > 0)
      Config.SetMoistureDryReading(p, dry);
    }
  return NULL;
  }
