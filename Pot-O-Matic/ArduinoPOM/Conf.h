//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#ifndef CONFIG_H
#define CONFIG_H

#include "EEPROM.h"

#define MAX_POT         2
#define MAX_NAME_LEN    50

enum SystemEventType
  {
  SE_PC_Reconnect,
  SE_PotNameUpdated,
  SE_DeviceNameUpdated,
  SE_MoistureReadingWet,
  SE_MoistureReadingDry,
  SE_PumpDurationUpdated,
  SE_WateringIntervalUpdated,
  SE_MoistureThresholdUpdated,

  SE_Max
  };

//*********************************** EEPROM Contents ******************************************

struct EepromContents
  {
  USHORT magicNumber;                   // Has to be equal to VALID_VALUE for memory to be considered sane
  char deviceName[MAX_NAME_LEN];        // Name of device to publish on BlueTooth
  USHORT waterDuration[MAX_POT];
  USHORT wateringInterval[MAX_POT];
  USHORT mositureThreshold[MAX_POT];
  USHORT mositureWetReading[MAX_POT];
  USHORT mositureDryReading[MAX_POT];
  char potName[MAX_POT][MAX_NAME_LEN];  // Names of all the pots
  };

//*********************************** Config ******************************************

class HardInit;

class Configuration
{
public:
  Configuration() { }

  const char *GetDeviceName() { return eeprom.deviceName; }
  const char *GetPotName(USHORT p) { return (p < MAX_POT) ? eeprom.potName[p] : ""; }
  USHORT GetWaterDuration(USHORT p) { return (p < MAX_POT) ? eeprom.waterDuration[p] : 0; }
  USHORT GetWateringInterval(USHORT p) { return (p < MAX_POT) ? eeprom.wateringInterval[p] : 0; }
  USHORT GetMoistureThreshold(USHORT p) { return (p < MAX_POT) ? eeprom.mositureThreshold[p] : 0; }
  USHORT GetMoistureWetReading(USHORT p) { return (p < MAX_POT) ? eeprom.mositureWetReading[p] : 0; }
  USHORT GetMoistureDryReading(USHORT p) { return (p < MAX_POT) ? eeprom.mositureDryReading[p] : 0; }

  void SetWaterDuration(USHORT p, USHORT num) { if (p < MAX_POT) eeprom.waterDuration[p] = num; DoSet(SE_PumpDurationUpdated, p); }
  void SetMoistureWetReading(USHORT p, USHORT num) { if (p < MAX_POT) eeprom.mositureWetReading[p] = num; DoSet(SE_MoistureReadingWet, p); }
  void SetMoistureDryReading(USHORT p, USHORT num) { if (p < MAX_POT) eeprom.mositureDryReading[p] = num; DoSet(SE_MoistureReadingDry, p); }
  void SetWateringInterval(USHORT p, USHORT num) { if (p < MAX_POT) eeprom.wateringInterval[p] = num; DoSet(SE_WateringIntervalUpdated, p); }
  void SetMoistureThreshold(USHORT p, USHORT num) { if (p < MAX_POT) eeprom.mositureThreshold[p] = num; DoSet(SE_MoistureThresholdUpdated, p); }
  const char *SetDeviceName(const char *name) { StrOp::Copy(eeprom.deviceName, name, MAX_NAME_LEN); DoSet(SE_DeviceNameUpdated); return eeprom.deviceName; }
  const char *SetPotName(USHORT p, const char *name) { if (p < MAX_POT) StrOp::Copy(eeprom.potName[p], name, MAX_NAME_LEN); DoSet(SE_PotNameUpdated, p); return eeprom.potName[p]; }

private:
  static EepromContents eeprom;

  static HardInit *hInit;                     // Hardware initialization
  static void HardwareInitialize();

  void DoSet(SystemEventType type, int n = -1);
};

extern Configuration Config;

#endif
