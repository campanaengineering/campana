//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#include "GalTam.h"

static short Offline[] = { 5, -5, 0 };
static short Reseting[] = { -1, 1, 0 };
static short OnlinePC[] = { 10, -3, 3, -3, 0 };
static short OnlineBlue[] = { 10, -1, 1, -1, 1, -1, 0 };
static short OnlineBluePC[] = { 10, 1, 1, -1, 1, -1, -3, 3, -3, 0 };
static short SOS[] = { 4, -2, 4, -2, 4, -6, 10, -2, 10, -2, 10, -6, 4, -2, 4, -2, 4, -32, 0 };

class HeartBeat : public Ticker
  {
  public:
    HeartBeat() : Ticker("HeartBeat", 20) { }

  protected:
    static HardInit *hInit;
    static void HardwareInitialize() { pinMode(LED_BUILTIN, OUTPUT);  digitalWrite(LED_BUILTIN, 1); }

    virtual void Tick()
      {
      short *pPat = NULL;
      static int countDown = 1;
      static short *pLastPat = NULL, *pBlink = NULL;

      if (millis() < 5 SECONDS)
        pPat = Reseting;
      else if (false)
        pPat = SOS;
      else if (BlueToothConnected && HostProtocol::Communicating())
        pPat = OnlineBluePC;
      else if (BlueToothConnected)
        pPat = OnlineBlue;
      else if (HostProtocol::Communicating())
        pPat = OnlinePC;
      else
        pPat = Offline;

      if (pPat != pLastPat)
        {
        countDown = 1;
        pBlink = pLastPat = pPat;
        }

      if (--countDown == 0)
        {
        while ((countDown = *pBlink++) == 0)
          pBlink = pPat;
        digitalWrite(LED_BUILTIN, countDown > 0);
        countDown = abs(countDown);
        }
      }
  } theBeat;

  HardInit *HeartBeat::hInit = new HardInit(HardwareInitialize);
