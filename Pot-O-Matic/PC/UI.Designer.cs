﻿
namespace Pot_O_Matic_Interface
{
    partial class UI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing && (components != null))
                {
                    components.Dispose();
                }
                base.Dispose(disposing);
            }
            catch { }
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Temperature = new System.Windows.Forms.Label();
            this.Humidity = new System.Windows.Forms.Label();
            this.CO2 = new System.Windows.Forms.Label();
            this.TVOC = new System.Windows.Forms.Label();
            this.Version = new System.Windows.Forms.Label();
            this.Hardware = new System.Windows.Forms.Label();
            this.Checksum = new System.Windows.Forms.Label();
            this.LogBox = new System.Windows.Forms.RichTextBox();
            this.ClearLog = new System.Windows.Forms.Button();
            this.PotGroup1 = new System.Windows.Forms.GroupBox();
            this.SimMoisture1 = new System.Windows.Forms.NumericUpDown();
            this.TargetMoisture1 = new System.Windows.Forms.NumericUpDown();
            this.WateringInterval1 = new System.Windows.Forms.NumericUpDown();
            this.WateringIntervalLabel1 = new System.Windows.Forms.Label();
            this.WaterDuration1 = new System.Windows.Forms.NumericUpDown();
            this.WaterDurationLabel1 = new System.Windows.Forms.Label();
            this.ForceWater1 = new System.Windows.Forms.Button();
            this.SecsTilWater1 = new System.Windows.Forms.Label();
            this.MoistureTargetLabel1 = new System.Windows.Forms.Label();
            this.CurrentMoisture1 = new System.Windows.Forms.Label();
            this.Watering1 = new System.Windows.Forms.TextBox();
            this.PotGroup2 = new System.Windows.Forms.GroupBox();
            this.SimMoisture2 = new System.Windows.Forms.NumericUpDown();
            this.TargetMoisture2 = new System.Windows.Forms.NumericUpDown();
            this.WateringInterval2 = new System.Windows.Forms.NumericUpDown();
            this.WateringIntervalLabel2 = new System.Windows.Forms.Label();
            this.WaterDuration2 = new System.Windows.Forms.NumericUpDown();
            this.WaterDurationLabel2 = new System.Windows.Forms.Label();
            this.ForceWater2 = new System.Windows.Forms.Button();
            this.SecsTilWater2 = new System.Windows.Forms.Label();
            this.MoistureTargetLabel2 = new System.Windows.Forms.Label();
            this.CurrentMoisture2 = new System.Windows.Forms.Label();
            this.Watering2 = new System.Windows.Forms.TextBox();
            this.DatascopeEnable = new System.Windows.Forms.CheckBox();
            this.ChangeAllNames = new System.Windows.Forms.Button();
            this.DeviceName = new System.Windows.Forms.Label();
            this.CalSensor = new System.Windows.Forms.Button();
            this.PotGroup1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SimMoisture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetMoisture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WateringInterval1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WaterDuration1)).BeginInit();
            this.PotGroup2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SimMoisture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetMoisture2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WateringInterval2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WaterDuration2)).BeginInit();
            this.SuspendLayout();
            // 
            // Temperature
            // 
            this.Temperature.AutoSize = true;
            this.Temperature.Location = new System.Drawing.Point(8, 85);
            this.Temperature.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Temperature.Name = "Temperature";
            this.Temperature.Size = new System.Drawing.Size(116, 20);
            this.Temperature.TabIndex = 0;
            this.Temperature.Text = "Temperature:";
            // 
            // Humidity
            // 
            this.Humidity.AutoSize = true;
            this.Humidity.Location = new System.Drawing.Point(166, 85);
            this.Humidity.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Humidity.Name = "Humidity";
            this.Humidity.Size = new System.Drawing.Size(83, 20);
            this.Humidity.TabIndex = 1;
            this.Humidity.Text = "Humidity:";
            // 
            // CO2
            // 
            this.CO2.AutoSize = true;
            this.CO2.Location = new System.Drawing.Point(344, 85);
            this.CO2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.CO2.Name = "CO2";
            this.CO2.Size = new System.Drawing.Size(49, 20);
            this.CO2.TabIndex = 2;
            this.CO2.Text = "CO2:";
            // 
            // TVOC
            // 
            this.TVOC.AutoSize = true;
            this.TVOC.Location = new System.Drawing.Point(454, 85);
            this.TVOC.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.TVOC.Name = "TVOC";
            this.TVOC.Size = new System.Drawing.Size(61, 20);
            this.TVOC.TabIndex = 4;
            this.TVOC.Text = "TVOC:";
            // 
            // Version
            // 
            this.Version.AutoSize = true;
            this.Version.Location = new System.Drawing.Point(8, 53);
            this.Version.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Version.Name = "Version";
            this.Version.Size = new System.Drawing.Size(75, 20);
            this.Version.TabIndex = 5;
            this.Version.Text = "Version:";
            // 
            // Hardware
            // 
            this.Hardware.AutoSize = true;
            this.Hardware.Location = new System.Drawing.Point(344, 53);
            this.Hardware.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Hardware.Name = "Hardware";
            this.Hardware.Size = new System.Drawing.Size(91, 20);
            this.Hardware.TabIndex = 6;
            this.Hardware.Text = "Hardware:";
            // 
            // Checksum
            // 
            this.Checksum.AutoSize = true;
            this.Checksum.Location = new System.Drawing.Point(166, 53);
            this.Checksum.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Checksum.Name = "Checksum";
            this.Checksum.Size = new System.Drawing.Size(97, 20);
            this.Checksum.TabIndex = 7;
            this.Checksum.Text = "Checksum:";
            // 
            // LogBox
            // 
            this.LogBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LogBox.Location = new System.Drawing.Point(3, 286);
            this.LogBox.Name = "LogBox";
            this.LogBox.ReadOnly = true;
            this.LogBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.LogBox.Size = new System.Drawing.Size(764, 405);
            this.LogBox.TabIndex = 8;
            this.LogBox.Text = "";
            // 
            // ClearLog
            // 
            this.ClearLog.Location = new System.Drawing.Point(668, 296);
            this.ClearLog.Name = "ClearLog";
            this.ClearLog.Size = new System.Drawing.Size(73, 29);
            this.ClearLog.TabIndex = 9;
            this.ClearLog.Text = "Clear";
            this.ClearLog.UseVisualStyleBackColor = true;
            this.ClearLog.Click += new System.EventHandler(this.ClearLog_Click);
            // 
            // PotGroup1
            // 
            this.PotGroup1.Controls.Add(this.SimMoisture1);
            this.PotGroup1.Controls.Add(this.TargetMoisture1);
            this.PotGroup1.Controls.Add(this.WateringInterval1);
            this.PotGroup1.Controls.Add(this.WateringIntervalLabel1);
            this.PotGroup1.Controls.Add(this.WaterDuration1);
            this.PotGroup1.Controls.Add(this.WaterDurationLabel1);
            this.PotGroup1.Controls.Add(this.ForceWater1);
            this.PotGroup1.Controls.Add(this.SecsTilWater1);
            this.PotGroup1.Controls.Add(this.MoistureTargetLabel1);
            this.PotGroup1.Controls.Add(this.CurrentMoisture1);
            this.PotGroup1.Controls.Add(this.Watering1);
            this.PotGroup1.Location = new System.Drawing.Point(12, 123);
            this.PotGroup1.Name = "PotGroup1";
            this.PotGroup1.Size = new System.Drawing.Size(366, 157);
            this.PotGroup1.TabIndex = 10;
            this.PotGroup1.TabStop = false;
            this.PotGroup1.Text = "Pot #1";
            this.PotGroup1.Visible = false;
            // 
            // SimMoisture1
            // 
            this.SimMoisture1.Location = new System.Drawing.Point(145, 19);
            this.SimMoisture1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SimMoisture1.Name = "SimMoisture1";
            this.SimMoisture1.Size = new System.Drawing.Size(63, 26);
            this.SimMoisture1.TabIndex = 17;
            this.SimMoisture1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.SimMoisture1.Value = new decimal(new int[] {
            55,
            0,
            0,
            0});
            this.SimMoisture1.Visible = false;
            this.SimMoisture1.ValueChanged += new System.EventHandler(this.SimMoisture1_ValueChanged);
            // 
            // TargetMoisture1
            // 
            this.TargetMoisture1.Location = new System.Drawing.Point(287, 20);
            this.TargetMoisture1.Name = "TargetMoisture1";
            this.TargetMoisture1.Size = new System.Drawing.Size(63, 26);
            this.TargetMoisture1.TabIndex = 7;
            this.TargetMoisture1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TargetMoisture1.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.TargetMoisture1.ValueChanged += new System.EventHandler(this.TargetMoisture1_ValueChanged);
            // 
            // WateringInterval1
            // 
            this.WateringInterval1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.WateringInterval1.Location = new System.Drawing.Point(261, 55);
            this.WateringInterval1.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.WateringInterval1.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.WateringInterval1.Name = "WateringInterval1";
            this.WateringInterval1.Size = new System.Drawing.Size(64, 26);
            this.WateringInterval1.TabIndex = 15;
            this.WateringInterval1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.WateringInterval1.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.WateringInterval1.ValueChanged += new System.EventHandler(this.WateringInterval1_ValueChanged);
            // 
            // WateringIntervalLabel1
            // 
            this.WateringIntervalLabel1.AutoSize = true;
            this.WateringIntervalLabel1.Location = new System.Drawing.Point(45, 58);
            this.WateringIntervalLabel1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.WateringIntervalLabel1.Name = "WateringIntervalLabel1";
            this.WateringIntervalLabel1.Size = new System.Drawing.Size(225, 20);
            this.WateringIntervalLabel1.TabIndex = 16;
            this.WateringIntervalLabel1.Text = "Interval between watering: ";
            // 
            // WaterDuration1
            // 
            this.WaterDuration1.Location = new System.Drawing.Point(141, 116);
            this.WaterDuration1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.WaterDuration1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WaterDuration1.Name = "WaterDuration1";
            this.WaterDuration1.Size = new System.Drawing.Size(54, 26);
            this.WaterDuration1.TabIndex = 12;
            this.WaterDuration1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.WaterDuration1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WaterDuration1.ValueChanged += new System.EventHandler(this.WaterDuration1_ValueChanged);
            // 
            // WaterDurationLabel1
            // 
            this.WaterDurationLabel1.AutoSize = true;
            this.WaterDurationLabel1.Location = new System.Drawing.Point(6, 119);
            this.WaterDurationLabel1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.WaterDurationLabel1.Name = "WaterDurationLabel1";
            this.WaterDurationLabel1.Size = new System.Drawing.Size(136, 20);
            this.WaterDurationLabel1.TabIndex = 13;
            this.WaterDurationLabel1.Text = "Water Duration:";
            // 
            // ForceWater1
            // 
            this.ForceWater1.Location = new System.Drawing.Point(238, 111);
            this.ForceWater1.Name = "ForceWater1";
            this.ForceWater1.Size = new System.Drawing.Size(87, 36);
            this.ForceWater1.TabIndex = 11;
            this.ForceWater1.Text = "Water";
            this.ForceWater1.UseVisualStyleBackColor = true;
            this.ForceWater1.Visible = false;
            this.ForceWater1.Click += new System.EventHandler(this.ForceWater1_Click);
            // 
            // SecsTilWater1
            // 
            this.SecsTilWater1.AutoSize = true;
            this.SecsTilWater1.Location = new System.Drawing.Point(83, 85);
            this.SecsTilWater1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.SecsTilWater1.Name = "SecsTilWater1";
            this.SecsTilWater1.Size = new System.Drawing.Size(187, 20);
            this.SecsTilWater1.TabIndex = 10;
            this.SecsTilWater1.Text = "Seconds until water: 0";
            // 
            // MoistureTargetLabel1
            // 
            this.MoistureTargetLabel1.AutoSize = true;
            this.MoistureTargetLabel1.Location = new System.Drawing.Point(223, 22);
            this.MoistureTargetLabel1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.MoistureTargetLabel1.Name = "MoistureTargetLabel1";
            this.MoistureTargetLabel1.Size = new System.Drawing.Size(66, 20);
            this.MoistureTargetLabel1.TabIndex = 8;
            this.MoistureTargetLabel1.Text = "Target:";
            // 
            // CurrentMoisture1
            // 
            this.CurrentMoisture1.AutoSize = true;
            this.CurrentMoisture1.Location = new System.Drawing.Point(18, 22);
            this.CurrentMoisture1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.CurrentMoisture1.Name = "CurrentMoisture1";
            this.CurrentMoisture1.Size = new System.Drawing.Size(165, 20);
            this.CurrentMoisture1.TabIndex = 6;
            this.CurrentMoisture1.Text = "Moisture Level: ???";
            // 
            // Watering1
            // 
            this.Watering1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Watering1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Watering1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Watering1.ForeColor = System.Drawing.Color.Crimson;
            this.Watering1.Location = new System.Drawing.Point(201, 117);
            this.Watering1.Name = "Watering1";
            this.Watering1.Size = new System.Drawing.Size(161, 24);
            this.Watering1.TabIndex = 14;
            this.Watering1.Text = "Watering";
            this.Watering1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Watering1.Visible = false;
            // 
            // PotGroup2
            // 
            this.PotGroup2.Controls.Add(this.SimMoisture2);
            this.PotGroup2.Controls.Add(this.TargetMoisture2);
            this.PotGroup2.Controls.Add(this.WateringInterval2);
            this.PotGroup2.Controls.Add(this.WateringIntervalLabel2);
            this.PotGroup2.Controls.Add(this.WaterDuration2);
            this.PotGroup2.Controls.Add(this.WaterDurationLabel2);
            this.PotGroup2.Controls.Add(this.ForceWater2);
            this.PotGroup2.Controls.Add(this.SecsTilWater2);
            this.PotGroup2.Controls.Add(this.MoistureTargetLabel2);
            this.PotGroup2.Controls.Add(this.CurrentMoisture2);
            this.PotGroup2.Controls.Add(this.Watering2);
            this.PotGroup2.Location = new System.Drawing.Point(397, 123);
            this.PotGroup2.Name = "PotGroup2";
            this.PotGroup2.Size = new System.Drawing.Size(366, 157);
            this.PotGroup2.TabIndex = 11;
            this.PotGroup2.TabStop = false;
            this.PotGroup2.Text = "Pot #2";
            this.PotGroup2.Visible = false;
            // 
            // SimMoisture2
            // 
            this.SimMoisture2.Location = new System.Drawing.Point(134, 19);
            this.SimMoisture2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.SimMoisture2.Name = "SimMoisture2";
            this.SimMoisture2.Size = new System.Drawing.Size(63, 26);
            this.SimMoisture2.TabIndex = 20;
            this.SimMoisture2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.SimMoisture2.Value = new decimal(new int[] {
            55,
            0,
            0,
            0});
            this.SimMoisture2.Visible = false;
            this.SimMoisture2.ValueChanged += new System.EventHandler(this.SimMoisture2_ValueChanged);
            // 
            // TargetMoisture2
            // 
            this.TargetMoisture2.Location = new System.Drawing.Point(271, 19);
            this.TargetMoisture2.Name = "TargetMoisture2";
            this.TargetMoisture2.Size = new System.Drawing.Size(63, 26);
            this.TargetMoisture2.TabIndex = 7;
            this.TargetMoisture2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TargetMoisture2.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.TargetMoisture2.ValueChanged += new System.EventHandler(this.TargetMoisture2_ValueChanged);
            // 
            // WateringInterval2
            // 
            this.WateringInterval2.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.WateringInterval2.Location = new System.Drawing.Point(254, 56);
            this.WateringInterval2.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.WateringInterval2.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.WateringInterval2.Name = "WateringInterval2";
            this.WateringInterval2.Size = new System.Drawing.Size(60, 26);
            this.WateringInterval2.TabIndex = 18;
            this.WateringInterval2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.WateringInterval2.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.WateringInterval2.ValueChanged += new System.EventHandler(this.WateringInterval2_ValueChanged);
            // 
            // WateringIntervalLabel2
            // 
            this.WateringIntervalLabel2.AutoSize = true;
            this.WateringIntervalLabel2.Location = new System.Drawing.Point(38, 58);
            this.WateringIntervalLabel2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.WateringIntervalLabel2.Name = "WateringIntervalLabel2";
            this.WateringIntervalLabel2.Size = new System.Drawing.Size(225, 20);
            this.WateringIntervalLabel2.TabIndex = 19;
            this.WateringIntervalLabel2.Text = "Interval between watering: ";
            // 
            // WaterDuration2
            // 
            this.WaterDuration2.Location = new System.Drawing.Point(143, 116);
            this.WaterDuration2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.WaterDuration2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WaterDuration2.Name = "WaterDuration2";
            this.WaterDuration2.Size = new System.Drawing.Size(54, 26);
            this.WaterDuration2.TabIndex = 15;
            this.WaterDuration2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.WaterDuration2.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.WaterDuration2.ValueChanged += new System.EventHandler(this.WaterDuration2_ValueChanged);
            // 
            // WaterDurationLabel2
            // 
            this.WaterDurationLabel2.AutoSize = true;
            this.WaterDurationLabel2.Location = new System.Drawing.Point(8, 119);
            this.WaterDurationLabel2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.WaterDurationLabel2.Name = "WaterDurationLabel2";
            this.WaterDurationLabel2.Size = new System.Drawing.Size(136, 20);
            this.WaterDurationLabel2.TabIndex = 16;
            this.WaterDurationLabel2.Text = "Water Duration:";
            // 
            // ForceWater2
            // 
            this.ForceWater2.Location = new System.Drawing.Point(239, 111);
            this.ForceWater2.Name = "ForceWater2";
            this.ForceWater2.Size = new System.Drawing.Size(87, 36);
            this.ForceWater2.TabIndex = 11;
            this.ForceWater2.Text = "Water";
            this.ForceWater2.UseVisualStyleBackColor = true;
            this.ForceWater2.Visible = false;
            this.ForceWater2.Click += new System.EventHandler(this.ForceWater2_Click);
            // 
            // SecsTilWater2
            // 
            this.SecsTilWater2.AutoSize = true;
            this.SecsTilWater2.Location = new System.Drawing.Point(77, 85);
            this.SecsTilWater2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.SecsTilWater2.Name = "SecsTilWater2";
            this.SecsTilWater2.Size = new System.Drawing.Size(187, 20);
            this.SecsTilWater2.TabIndex = 10;
            this.SecsTilWater2.Text = "Seconds until water: 0";
            // 
            // MoistureTargetLabel2
            // 
            this.MoistureTargetLabel2.AutoSize = true;
            this.MoistureTargetLabel2.Location = new System.Drawing.Point(207, 22);
            this.MoistureTargetLabel2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.MoistureTargetLabel2.Name = "MoistureTargetLabel2";
            this.MoistureTargetLabel2.Size = new System.Drawing.Size(66, 20);
            this.MoistureTargetLabel2.TabIndex = 8;
            this.MoistureTargetLabel2.Text = "Target:";
            // 
            // CurrentMoisture2
            // 
            this.CurrentMoisture2.AutoSize = true;
            this.CurrentMoisture2.Location = new System.Drawing.Point(8, 22);
            this.CurrentMoisture2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.CurrentMoisture2.Name = "CurrentMoisture2";
            this.CurrentMoisture2.Size = new System.Drawing.Size(165, 20);
            this.CurrentMoisture2.TabIndex = 6;
            this.CurrentMoisture2.Text = "Moisture Level: ???";
            // 
            // Watering2
            // 
            this.Watering2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.Watering2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Watering2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Watering2.ForeColor = System.Drawing.Color.Crimson;
            this.Watering2.Location = new System.Drawing.Point(203, 117);
            this.Watering2.Name = "Watering2";
            this.Watering2.Size = new System.Drawing.Size(161, 24);
            this.Watering2.TabIndex = 17;
            this.Watering2.Text = "Watering";
            this.Watering2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.Watering2.Visible = false;
            // 
            // DatascopeEnable
            // 
            this.DatascopeEnable.AutoSize = true;
            this.DatascopeEnable.Location = new System.Drawing.Point(566, 84);
            this.DatascopeEnable.Name = "DatascopeEnable";
            this.DatascopeEnable.Size = new System.Drawing.Size(79, 24);
            this.DatascopeEnable.TabIndex = 12;
            this.DatascopeEnable.Text = "Scope";
            this.DatascopeEnable.UseVisualStyleBackColor = true;
            this.DatascopeEnable.CheckedChanged += new System.EventHandler(this.DatascopeEnable_CheckedChanged);
            // 
            // ChangeAllNames
            // 
            this.ChangeAllNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChangeAllNames.Location = new System.Drawing.Point(668, 72);
            this.ChangeAllNames.Name = "ChangeAllNames";
            this.ChangeAllNames.Size = new System.Drawing.Size(91, 45);
            this.ChangeAllNames.TabIndex = 15;
            this.ChangeAllNames.Text = "Change Names";
            this.ChangeAllNames.UseVisualStyleBackColor = true;
            this.ChangeAllNames.Visible = false;
            this.ChangeAllNames.Click += new System.EventHandler(this.ChangeAllNames_Click);
            // 
            // DeviceName
            // 
            this.DeviceName.AutoSize = true;
            this.DeviceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviceName.Location = new System.Drawing.Point(12, 9);
            this.DeviceName.Name = "DeviceName";
            this.DeviceName.Size = new System.Drawing.Size(170, 31);
            this.DeviceName.TabIndex = 16;
            this.DeviceName.Text = "Pot-O-Matic";
            // 
            // CalSensor
            // 
            this.CalSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalSensor.Location = new System.Drawing.Point(668, 7);
            this.CalSensor.Name = "CalSensor";
            this.CalSensor.Size = new System.Drawing.Size(91, 45);
            this.CalSensor.TabIndex = 17;
            this.CalSensor.Text = "Calibrate Sensor";
            this.CalSensor.UseVisualStyleBackColor = true;
            this.CalSensor.Visible = false;
            this.CalSensor.Click += new System.EventHandler(this.CalSensor_Click);
            // 
            // UI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(771, 692);
            this.Controls.Add(this.CalSensor);
            this.Controls.Add(this.DeviceName);
            this.Controls.Add(this.ChangeAllNames);
            this.Controls.Add(this.DatascopeEnable);
            this.Controls.Add(this.TVOC);
            this.Controls.Add(this.CO2);
            this.Controls.Add(this.Humidity);
            this.Controls.Add(this.Temperature);
            this.Controls.Add(this.PotGroup2);
            this.Controls.Add(this.PotGroup1);
            this.Controls.Add(this.ClearLog);
            this.Controls.Add(this.LogBox);
            this.Controls.Add(this.Checksum);
            this.Controls.Add(this.Hardware);
            this.Controls.Add(this.Version);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "UI";
            this.Text = "Pot-O-Matic";
            this.PotGroup1.ResumeLayout(false);
            this.PotGroup1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SimMoisture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetMoisture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WateringInterval1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WaterDuration1)).EndInit();
            this.PotGroup2.ResumeLayout(false);
            this.PotGroup2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SimMoisture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TargetMoisture2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WateringInterval2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WaterDuration2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Temperature;
        private System.Windows.Forms.Label Humidity;
        private System.Windows.Forms.Label CO2;
        private System.Windows.Forms.Label TVOC;
        private System.Windows.Forms.Label Version;
        private System.Windows.Forms.Label Hardware;
        private System.Windows.Forms.Label Checksum;
        private System.Windows.Forms.RichTextBox LogBox;
        private System.Windows.Forms.Button ClearLog;
        public System.Windows.Forms.Button ForceWater1;
        public System.Windows.Forms.Label SecsTilWater1;
        public System.Windows.Forms.Label MoistureTargetLabel1;
        public System.Windows.Forms.NumericUpDown TargetMoisture1;
        public System.Windows.Forms.Label CurrentMoisture1;
        public System.Windows.Forms.Button ForceWater2;
        public System.Windows.Forms.Label SecsTilWater2;
        public System.Windows.Forms.Label MoistureTargetLabel2;
        public System.Windows.Forms.NumericUpDown TargetMoisture2;
        public System.Windows.Forms.Label CurrentMoisture2;
        private System.Windows.Forms.CheckBox DatascopeEnable;
        public System.Windows.Forms.Label WaterDurationLabel1;
        public System.Windows.Forms.NumericUpDown WaterDuration1;
        private System.Windows.Forms.TextBox Watering1;
        public System.Windows.Forms.NumericUpDown WaterDuration2;
        public System.Windows.Forms.Label WaterDurationLabel2;
        private System.Windows.Forms.TextBox Watering2;
        public System.Windows.Forms.NumericUpDown WateringInterval1;
        public System.Windows.Forms.Label WateringIntervalLabel1;
        public System.Windows.Forms.NumericUpDown WateringInterval2;
        public System.Windows.Forms.Label WateringIntervalLabel2;
        public System.Windows.Forms.NumericUpDown SimMoisture1;
        public System.Windows.Forms.NumericUpDown SimMoisture2;
        private System.Windows.Forms.Button ChangeAllNames;
        public System.Windows.Forms.GroupBox PotGroup1;
        public System.Windows.Forms.GroupBox PotGroup2;
        public System.Windows.Forms.Label DeviceName;
        private System.Windows.Forms.Button CalSensor;
    }
}

