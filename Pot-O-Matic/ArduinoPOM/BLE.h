//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#ifndef BLE_H
#define BLE_H

#include <ArduinoBLE.h>

#define SYSTEM_NAME                         "Pot-O-Matic"
#define SYSTEM_UUID                         "E14F73A3-B214-4596-B43A-A9683CC3A3DC"

#define CHAR_VAL_MAX_WIDTH                  50
#define DESCRIPTOR_UUID_BASE                0x1000
#define ADDITIONAL_SERVICE_UUID_BASE        0x9001

enum ServiceCharacteristic
  {
  SC_Type = 1,
  SC_Index,
  };

enum SystemServiceCharacteristic
  {
  SSC_Version = 3,
  SSC_Hardware,
  SSC_Checksum,
  SSC_DeviceName,
  SSC_Temperature,
  SSC_Humidity,
  SSC_CO2,
  SSC_TVOC
  };

class Characteristic;

extern bool BlueToothConnected;             // True when external device connected via bluetooth
extern bool BlueToothOperational;           // True when bluetooth successfully initialized
extern const char *ToUUID(int uuid);

// ###################################################### Service class ######################################################
class Service : public Obj
{
public:
  Service(const char *type, int index = 0) : Obj(), advertised(false), bleService(ToUUID(nextUUID++)) { FinishConstruction(type, index, this); }
  Service(const char *type, const char *uuid, int index = 0) : Obj(), advertised(true), bleService(uuid) { FinishConstruction(type, index, this); }

  static void FinishInitialization();
  static Service *GetSystemService();

  void AddCharacteristic(BLECharacteristic& character) { bleService.addCharacteristic(character); }

private:
  bool advertised;                          // True if this is the primary service to be advertised to the world
  BLEService bleService;                    // Access to BLE infrastructure

  static List *list;                        // All defined characteristics
  static int nextUUID;                      // UUID to be assigned to next service
  static Service *systemService;            // The system service

  static HostCommand DN_Cmd;                // Support setting device name command
  static Characteristic *blueDeviceName;    // Characteristic that contains the device name
  static void SetDeviceName(const char *name);
  static Str *DN_Func(char *msg) { SetDeviceName(msg + 2); return NULL; }
  static void FinishConstruction(const char *type, int index, Service *toAdd);
  static void BlueUpdatedDeviceName(void *ignored, const char *name) { SetDeviceName(name); }


  virtual ObjType GetObjType() { return Type_BLE_Service; }

  /// Indicate whether object should be persisted after being used as data in a SysEvent when publication is complete
  virtual bool Persist() { return true; }
};

// ###################################################### Characteristic class ######################################################
class Characteristic : public Obj
{
public:
  // Constructor for numeric value
  Characteristic(int index, int uuid, const char *name, Service *serv, uint8_t properties, int initValue, void (*func)(void *, const char *) = NULL, void *obj = NULL);
  // Constructor for string value
  Characteristic(int index, int uuid, const char *name, Service *serv, uint8_t properties, const char *initValue = "0", void (*func)(void *, const char *) = NULL, void *obj = NULL);

  void Write(int num);
  void Write(const char *src);
  int AddDescriptor(const char *contents);
  void FinishConstruction(int index, int uuid, const char *name, Service *serv, uint8_t properties);

private:
  void *setObj;
  uint8_t val[CHAR_VAL_MAX_WIDTH + 1];
  BLECharacteristic *bleCharacteristic;     // Access to BLE infrastructure
  void (*setFunc)(void *, const char *);

  static List *list;                        // All defined characteristics
  static int nextDescriptorIndex;           // Index assigned to next descriptor created

  static void written(BLEDevice central, BLECharacteristic characteristic);

  virtual ObjType GetObjType() { return Type_BLE_Characteristic; }

  /// Indicate whether object should be persisted after being used as data in a SysEvent when publication is complete
  virtual bool Persist() { return true; }
};

#endif
