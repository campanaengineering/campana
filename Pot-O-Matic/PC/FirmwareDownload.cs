﻿//-----------------------------------------------------------------------
// <copyright file="FirmwareDownload.cs" company="Coinstar LLC.">
//     Copyright (C) Coinstar LLC.  All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

#region Using

using System;
using System.IO;
using System.IO.Ports;
using System.Threading;

#endregion

namespace Pot_O_Matic_Interface
{
    #region Enumerations

    /// <summary>
    /// Packet command types
    /// </summary>
    public enum SVL_Command
    {
        Error,
        Version,
        EnterBootloader,
        RequestNextFrame,
        FirmwareFrame,
        ResendFrame,
        Done
    }

    #endregion

    /// <summary>
    /// Artemis firmware download
    /// </summary>
    public class FirmwareDownload : IDisposable
    {
        #region Variables

        /// <summary>
        /// The serial port
        /// </summary>
        private SerialPort comPort = null;

        /// <summary>
        /// Which COM port to use
        /// </summary>
        private static string comPortName = null;

        /// <summary>
        /// Name of file to download
        /// </summary>
        private string firmwareFile = string.Empty;

        /// <summary>
        /// Maximum amount of data to send per frame
        /// </summary>
        public const int FrameSize = 2048;

        /// <summary>
        /// Packet lengths
        /// </summary>
        public const int VersionPacketLength = 6;
        public const int CommandPacketLength = 5;

        #endregion

        #region Startup/Shutdown

        /// <summary>
        /// FirmwareDownload constructor
        /// </summary>
        private FirmwareDownload(string binary, string portName)
        {
            firmwareFile = binary;
            comPortName = portName;
        }

        /// <summary>
        /// Destructor for Protocol
        /// </summary>
        ~FirmwareDownload()
        {
            Dispose();
        }

        /// <summary>
        /// Dispose the object
        /// </summary>
        public void Dispose()
        {
            if (comPort != null)
            {
                comPort.DtrEnable = false;
                comPort.Close();
            }
        }

        /// <summary>
        /// Perform download
        /// </summary>
        public static bool Start(string binary, string portName)
        {
            bool rc;
            FirmwareDownload instance = new FirmwareDownload(binary, portName);

            rc = instance.Execute();

            instance.Dispose();
            return rc;
        }

        #endregion

        #region Send/Receive

        /// <summary>
        /// Send a packet to the Artemis boot loader
        /// </summary>
        /// <param name="cmd">Type of command</param>
        /// <param name="data">If included data, it will be a portion of this byte array</param>
        /// <param name="offset">Offset into byte array to start sending data from</param>
        /// <param name="length">Number of bytes starting at offset to send</param>
        public void SendPacket(SVL_Command cmd, byte[] data = null, int offset = 0, int length = 0)
        {
            ushort crc;
            int off = 0, dLen = length + 3;
            byte[] packet = new byte[length + 5];

            packet[off++] = (byte)(dLen >> 8);
            packet[off++] = (byte)dLen;
            packet[off++] = (byte)cmd;

            if (data != null)
                for (int i = 0; i < length; i++)
                    packet[off++] = data[offset++];

            crc = CalcCRC(packet, 2, length + 1);
            packet[off++] = (byte)(crc >> 8);
            packet[off++] = (byte)crc;

            comPort.Write(packet, 0, packet.Length);
            if (length == 0)
                Log(string.Format("Sending {0}", cmd));
            else
                Log(string.Format("Sending {0} Length {1}", cmd, length));
        }

        /// <summary>
        /// Receive a packet from Artemis boot loader
        /// </summary>
        /// <param name="bytesExpected">Number of bytes in packet being received</param>
        /// <returns>Type of packet</returns>
        public SVL_Command ReceivePacket(int bytesExpected)
        {
            try
            {
                DateTime start = DateTime.Now;
                byte[] packet = new byte[bytesExpected];

                while (comPort.BytesToRead < bytesExpected)
                    if ((DateTime.Now - start).TotalSeconds > .5)
                    {
                        Log("Receive packet timeout");
                        return SVL_Command.Error;
                    }
                    else
                        Thread.Sleep(5);

                comPort.Read(packet, 0, bytesExpected);

                int length = (packet[0] << 8) + packet[1];
                if (length < 2)
                    return SVL_Command.Error;

                if (CalcCRC(packet, 2, length) != 0)
                    Log("CRC Error");
                else
                    return (SVL_Command)packet[2];
            }
            catch (Exception e)
            {
                Log("ReceivePacket exception " + e.Message);
            }
            return SVL_Command.Error;
        }

        #endregion

        #region Execute

        private bool Execute()
        {
            try
            {
                byte[] baudDetect = { (byte)'U' };                      // Magic byte that allows boot loader to determine baud rate
                byte[] firmware = File.ReadAllBytes(firmwareFile);      // Read the entire binary into a byte array

                for (int attempt = 0; attempt < 3; attempt++)
                {
                    int resendCount = 0;
                    bool succeeding = true;
                    int offset = -FrameSize;        // Init to -frameSize so first frame request increments it to zero

                    if (!InitializePort())
                    {
                        Log("Can't open COM port");
                        continue;
                    }

                    Thread.Sleep(150);
                    comPort.Write(baudDetect, 0, 1);
                    Log("Port open, sending baud char");

                    if (ReceivePacket(VersionPacketLength) != SVL_Command.Version)
                    {
                        Log("No/Invalid response from loader");
                        continue;
                    }

                    SendPacket(SVL_Command.EnterBootloader);

                    while (succeeding)
                    {
                        switch (ReceivePacket(CommandPacketLength))
                        {
                        case SVL_Command.Error:
                            succeeding = false;
                            break;

                        case SVL_Command.RequestNextFrame:
                            resendCount = 0;
                            offset += FrameSize;
                            break;

                        case SVL_Command.ResendFrame:
                            if (++resendCount > 3)
                            {
                                succeeding = false;
                                Log("Too many frame retries");
                            }
                            else
                                Log("Frame REJECTED, retry");
                            break;
                        }

                        // If no error, send next frame (or done)
                        if (succeeding)
                            if (offset < firmware.Length)
                                SendPacket(SVL_Command.FirmwareFrame, firmware, offset, (firmware.Length - offset > FrameSize) ? FrameSize : firmware.Length - offset);
                            else
                            {
                                SendPacket(SVL_Command.Done);
                                return true;
                            }
                    }
                }
            }
            catch (Exception e)
            {
                Log(string.Format("Exception '{0}'", e.Message));
            }
            return false;
        }

        #endregion

        #region Tools

        /// <summary>
        /// Initialize COM port
        /// </summary>
        public bool InitializePort()
        {
            while (Protocol.PortStillOpen)
                Thread.Sleep(100);

            if (comPort != null)
            {
                comPort.DtrEnable = false;
                comPort.Close();
                Thread.Sleep(1000);
            }

            comPort = (comPortName != null) ? new SerialPort(comPortName, 921600, Parity.None, 8, StopBits.One) : null;

            if (comPort != null)
            {
                try
                {
                    comPort.DtrEnable = true;
                    comPort.Handshake = Handshake.None;
                    comPort.Open();
                    comPort.DiscardInBuffer();

                    return true;
                }
                catch
                {
                }
                comPort.DtrEnable = false;
                comPort.Close();
                comPort = null;
            }
            return false;
        }

        /// <summary>
        /// Log some text
        /// </summary>
        /// <param name="text">Text to log</param>
        private static void Log(string text)
        {
            if (Executive.Instance != null)
                Executive.Instance.Log(text);
        }

        private static ushort[] crctab =
        {
            0x0000, 0x8005, 0x800F, 0x000A, 0x801B, 0x001E, 0x0014, 0x8011,
            0x8033, 0x0036, 0x003C, 0x8039, 0x0028, 0x802D, 0x8027, 0x0022,
            0x8063, 0x0066, 0x006C, 0x8069, 0x0078, 0x807D, 0x8077, 0x0072,
            0x0050, 0x8055, 0x805F, 0x005A, 0x804B, 0x004E, 0x0044, 0x8041,
            0x80C3, 0x00C6, 0x00CC, 0x80C9, 0x00D8, 0x80DD, 0x80D7, 0x00D2,
            0x00F0, 0x80F5, 0x80FF, 0x00FA, 0x80EB, 0x00EE, 0x00E4, 0x80E1,
            0x00A0, 0x80A5, 0x80AF, 0x00AA, 0x80BB, 0x00BE, 0x00B4, 0x80B1,
            0x8093, 0x0096, 0x009C, 0x8099, 0x0088, 0x808D, 0x8087, 0x0082,
            0x8183, 0x0186, 0x018C, 0x8189, 0x0198, 0x819D, 0x8197, 0x0192,
            0x01B0, 0x81B5, 0x81BF, 0x01BA, 0x81AB, 0x01AE, 0x01A4, 0x81A1,
            0x01E0, 0x81E5, 0x81EF, 0x01EA, 0x81FB, 0x01FE, 0x01F4, 0x81F1,
            0x81D3, 0x01D6, 0x01DC, 0x81D9, 0x01C8, 0x81CD, 0x81C7, 0x01C2,
            0x0140, 0x8145, 0x814F, 0x014A, 0x815B, 0x015E, 0x0154, 0x8151,
            0x8173, 0x0176, 0x017C, 0x8179, 0x0168, 0x816D, 0x8167, 0x0162,
            0x8123, 0x0126, 0x012C, 0x8129, 0x0138, 0x813D, 0x8137, 0x0132,
            0x0110, 0x8115, 0x811F, 0x011A, 0x810B, 0x010E, 0x0104, 0x8101,
            0x8303, 0x0306, 0x030C, 0x8309, 0x0318, 0x831D, 0x8317, 0x0312,
            0x0330, 0x8335, 0x833F, 0x033A, 0x832B, 0x032E, 0x0324, 0x8321,
            0x0360, 0x8365, 0x836F, 0x036A, 0x837B, 0x037E, 0x0374, 0x8371,
            0x8353, 0x0356, 0x035C, 0x8359, 0x0348, 0x834D, 0x8347, 0x0342,
            0x03C0, 0x83C5, 0x83CF, 0x03CA, 0x83DB, 0x03DE, 0x03D4, 0x83D1,
            0x83F3, 0x03F6, 0x03FC, 0x83F9, 0x03E8, 0x83ED, 0x83E7, 0x03E2,
            0x83A3, 0x03A6, 0x03AC, 0x83A9, 0x03B8, 0x83BD, 0x83B7, 0x03B2,
            0x0390, 0x8395, 0x839F, 0x039A, 0x838B, 0x038E, 0x0384, 0x8381,
            0x0280, 0x8285, 0x828F, 0x028A, 0x829B, 0x029E, 0x0294, 0x8291,
            0x82B3, 0x02B6, 0x02BC, 0x82B9, 0x02A8, 0x82AD, 0x82A7, 0x02A2,
            0x82E3, 0x02E6, 0x02EC, 0x82E9, 0x02F8, 0x82FD, 0x82F7, 0x02F2,
            0x02D0, 0x82D5, 0x82DF, 0x02DA, 0x82CB, 0x02CE, 0x02C4, 0x82C1,
            0x8243, 0x0246, 0x024C, 0x8249, 0x0258, 0x825D, 0x8257, 0x0252,
            0x0270, 0x8275, 0x827F, 0x027A, 0x826B, 0x026E, 0x0264, 0x8261,
            0x0220, 0x8225, 0x822F, 0x022A, 0x823B, 0x023E, 0x0234, 0x8231,
            0x8213, 0x0216, 0x021C, 0x8219, 0x0208, 0x820D, 0x8207, 0x0202
        };

        private ushort CalcCRC(byte[] data, int offset, int length)
        {
            ushort crc = 0, t;

            for (int i = 0; i < length; i++)
            {
                t = crctab[data[offset + i] ^ (crc >> 8)];
                crc = (ushort)((((t >> 8) ^ (byte)crc) << 8) | (byte)t);
            }

            return crc;
        }

        #endregion
    }
}
