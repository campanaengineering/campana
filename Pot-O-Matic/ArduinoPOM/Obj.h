//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#ifndef OBJ_H
#define OBJ_H

/// All of the Obj types
enum ObjType
{
	Type_Null = 0,
    Type_Int,
    Type_UInt,
	Type_Bool,
	Type_Func,
    Type_Queue,
	Type_Ticker,
	Type_String,
	Type_ByteArray,
	Type_BLE_Service,
    Type_BLE_Characteristic,
};

/// All exceptions thrown by the expression evaluator use this
struct Up			// Type to use for throwing errors
{
	Up(const char *format = NULL, ...);
	char text[100];
};

class List;

/// Base class for generic objects
class Obj
{
public:
    /// Obj constructor
    Obj() : next(NULL), inSomeList(false) {}

    /// Obj destructor
    virtual ~Obj() { }

    friend List;

    /// Get type of object
    /// \return Object type
    virtual ObjType GetObjType() { return Type_Null; }

    /// Convert value of object to a string
    virtual int ToString(char *dest) { return 0; }

    /// Indicate whether object should be persisted after being used as data in a SysEvent when publication is complete
    virtual bool Persist() { return false; }

    /// Object no longer needed, dispose of as appropriate for object type
    virtual void Done() { if (!Persist()) delete this; }

    /// The next item in the list
    /// \return Next item in list
    inline Obj *Next() { return next; }

    /// True if in a list
    inline bool InSomeList() { return inSomeList; }

protected:
    Obj *next;                 ///< Pointer to next item in linked list
    bool inSomeList;            ///< True while in a list somewhere

    /// When sorting list, use this function for comparison
    /// \return True if insert new Item here
    virtual bool InsertHere(Obj *inList)   { return true; }
};

/// Object that is not discarded after being used
class KeptObj : public Obj
  {
  virtual bool Persist() { return true; }
  };

/// Object to hold the value of an integer
class Integer : public Obj
{
public:
    Integer(int val) : Obj(), value(val) {}

    virtual ObjType GetObjType() { return Type_Int; }

    virtual void Done();
    virtual int ToString(char *dest) { return StrOp::IntToString(dest, value); }

    inline int Value() { return value; }

    static Integer *Alloc(int val);

protected:
    int value;

    static List freeList;
};

/// Object to hold the value of a boolean in a list
class Boolean : public Obj
{
public:
    Boolean(bool val) : Obj(), value(val) {}

    virtual ObjType GetObjType() { return Type_Bool; }

    virtual int ToString(char *dest) { return StrOp::Copy(dest, value ? StrOp::True : StrOp::False); }

    inline bool Value() { return value; }

protected:
    bool value;
};

#define LOCAL_STR_SIZE      100

/// Class used to represent a string
class Str : public Obj
{
public:
    Str(const char *val, bool keep = false);
    virtual ~Str() { Free(); }

    virtual void Done();
    virtual ObjType GetObjType() { return Type_String; }
    void Free() { if (value && value != local) value = StrOp::efree(value); }
    virtual int ToString(char *dest) { return StrOp::Copy(dest, value, 200); }

    inline const char *Value() { return value; }

    static Str *Alloc(const char *val, bool keep = false);

protected:
    char *value;
    bool keepAround;
    char local[LOCAL_STR_SIZE + 1];

    static List freeList;

    /// If just want alphabetical strings...
    virtual bool InsertHere(Obj *inList) { return strcmp(value, ((Str *)inList)->value) <= 0; }
};

/// Class used to represent a byte array
class ByteArray : public Obj
{
public:
    /// ByteArray constructor
    ByteArray(int len, const UC *array = NULL) : Obj(), length(len) { byteArray = new UC[len]; if (array) memcpy((void *)byteArray, array, len); }

    /// ByteArray destructor
    virtual ~ByteArray() { Free(); }

    /// Get type of object
    /// \return Object type
    virtual ObjType GetObjType() { return Type_ByteArray; }

    /// Get array
    inline UC *Address() { return byteArray; };

    /// Get Length of array
    inline int Length() { return length; }

    /// Free array
    void Free() { if (byteArray) { delete [] byteArray; byteArray = NULL; } }

protected:
    int length;
    UC *byteArray;               ///< Byte array stored here
};

/// Object to hold the value of an unsigned long
class ULongInt : public Obj
{
public:
  ULongInt(Obj *pObj);
  ULongInt(int val) : Obj(), value(val) {}

  virtual ObjType GetObjType() { return Type_UInt; }
  virtual int ToString(char *dest) { return StrOp::IntToString(dest, value); }

  inline ULONG Value() { return value; }

protected:
  ULONG value;
};

#endif
