//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#include "GalTam.h"

//******************************************** Ticker Support *********************************************

List *Ticker::tickers = NULL;

Ticker::Ticker(const char *nme, int tps, OrderOfExecution pri) : Obj(), interval(SYSTEM_TPS / tps), order(pri)
  {
  if (tickers == NULL)
    tickers = new List();

  tickers->AddInOrder(this);
  nextTick = (tps < 0) ? 0 : 1;             // If tps < 0 indicate never call ticker
  strncpy(name, nme, sizeof(name)-1);
  }

void Ticker::PerformInitialization()
  {
      // First call all the hardware initialization code
  HardInit::PerformInitialization();

      // Second call all the generic initialization code
  if (tickers)
    for (Ticker *pTick = (Ticker *)tickers->First(); pTick; pTick = (Ticker *)pTick->next)
      {
//      Serial.printf("Init: %s\n", pTick->name);
      pTick->Initialize();
      }

      // Finally set all the tick routines to start 100ms from now
  if (tickers)
    for (Ticker *pTick = (Ticker *)tickers->First(); pTick; pTick = (Ticker *)pTick->next)
      pTick->nextTick = (pTick->nextTick) ? millis() + 100 : 0xffffffff;
  }

void Ticker::PollClients()
  {
  if (tickers)
    for (Ticker *pTick = (Ticker *)tickers->First(); pTick; pTick = (Ticker *)pTick->next)
      if (millis() < pTick->nextTick)
        pTick->Poll();
      else
        {
//        Serial.printf("Tick: %s\n", pTick->name);
        pTick->Tick();
        pTick->nextTick = millis() + pTick->interval;
        }
  }

void Ticker::SendSystemEvent(SystemEventType type, int n)
  {
  if (tickers)
    for (Ticker *pTick = (Ticker *)tickers->First(); pTick; pTick = (Ticker *)pTick->next)
      pTick->SystemEvent(type, n);
  }

//******************************************** HardInit Support *********************************************

List *HardInit::list = NULL;

/// Constructor for hardware initialization
HardInit::HardInit(
    void (*initFunc)(),                             ///< Function to execute
    OrderOfExecution ord                            ///< Order of initialization
) : Obj(), order(ord), func(initFunc)
  {
  if (list == NULL)
    list = new List();

  list->AddInOrder(this);
  }

/// Call all the hardware initializers
void HardInit::PerformInitialization()
  {
  if (list)
    for (HardInit *pInit = (HardInit *)list->First(); pInit; pInit = (HardInit *)pInit->next)
      pInit->func();
  }

