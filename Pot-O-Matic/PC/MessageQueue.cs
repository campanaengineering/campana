﻿#region Using

using System;
using System.Threading;
using System.Collections.Generic;

#endregion

namespace Pot_O_Matic_Interface
{
    /// <summary>
    /// Support a message queue
    /// </summary>
    public class MessageQueue<T> : IDisposable
    {
        /// <summary>
        /// Maximum items in queue
        /// </summary>
        private readonly int _QUEUE_SIZE;

        /// <summary>
        /// A semaphore object is used to notify enqueue happend signal to the dequeue subrouting
        /// </summary>
        public Semaphore Sem { get; private set; }

        /// <summary>
        /// An internal queue data structure holds the messages in a queue
        /// </summary>
        private Queue<T> internalQueue;

        /// <summary>
        /// A private object that is used to acquire a lock on it to ensure synchronous execution of some operations in multithreaded environment
        /// </summary>
        private object syncLock;

        /// <summary>
        /// Construct the message queue object with the maximum size limit. If no of messages in the queue meets the maximum size of the queue, any
        /// subsequent enqueue will be discareded. i.e. those message will be lost until you dequeue any message i.e. provide a room for new message to enter the queue.
        /// </summary>
        /// <param name="queueSize">Maximum items in queue.</param>
        public MessageQueue(int queueSize = 10)
        {
            syncLock = new object();
            _QUEUE_SIZE = queueSize;
            Sem = new Semaphore(0, _QUEUE_SIZE);
            internalQueue = new Queue<T>(_QUEUE_SIZE);
        }

        /// <summary>
        /// Get current queue contents.  Replace internal queue with new one.
        /// </summary>
        public Queue<T> Contents
        {
            get
            {
                Queue<T> queue = null;

                lock (syncLock)
                {
                    if (internalQueue.Count != 0)
                    {
                        queue = internalQueue;
                        Sem = new Semaphore(0, _QUEUE_SIZE);
                        internalQueue = new Queue<T>(_QUEUE_SIZE);
                    }
                }

                return queue;
            }
        }

        /// <summary>
        /// Enqueue message in to the Message Queue
        /// </summary>
        /// <param name="message">Message to enqueue.</param>
        /// <returns>Element that was passed</returns>
        public T Enqueue(T message)
        {
            lock (syncLock)
            {
                if (message != null)
                {
                    // now enqueue the message in to the internal queue data structure
                    internalQueue.Enqueue(message);

                    // try to provide a room in the semaphore so that DequeueMessage
                    // can enter into it
                    Sem.Release();
                }
            }
            return message;
        }

        /// <summary>
        /// Dequeue message from the Message Queue.  It is assumed that the semqphore has been aquired prior to this call
        /// </summary>
        /// <returns>Return and remove top item from queue.</returns>
        public T Dequeue()
        {
            lock (syncLock)
            {
                return internalQueue.Dequeue();
            }
        }

        /// <summary>
        /// True if queue is empty
        /// </summary>
        public bool Empty { get { lock (syncLock) return internalQueue.Count == 0; } }

        /// <summary>
        /// Clear out all items in queue
        /// </summary>
        public void Clear()
        {
            lock (syncLock)
            {
                if (internalQueue.Count != 0)
                {
                    Sem.Dispose();
                    internalQueue.Clear();
                    Sem = new Semaphore(0, _QUEUE_SIZE);
                }
            }
        }

        /// <summary>
        /// Dispose the Message Queue object
        /// </summary>
        public void Dispose()
        {
            // if the semaphore is not null, close it and set it to null
            if (Sem != null)
            {
                Sem.Close();
                Sem = null;
            }
            // clear the items of the internal queue
            internalQueue.Clear();
        }
    }
}
