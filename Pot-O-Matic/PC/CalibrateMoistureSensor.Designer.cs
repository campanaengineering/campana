﻿
namespace Pot_O_Matic_Interface
{
    partial class CalibrateMoistureSensor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SelectedPot = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BeginCal = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.Instructions = new System.Windows.Forms.TextBox();
            this.Reading = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // SelectedPot
            // 
            this.SelectedPot.FormattingEnabled = true;
            this.SelectedPot.Location = new System.Drawing.Point(91, 11);
            this.SelectedPot.Margin = new System.Windows.Forms.Padding(5);
            this.SelectedPot.Name = "SelectedPot";
            this.SelectedPot.Size = new System.Drawing.Size(288, 28);
            this.SelectedPot.TabIndex = 0;
            this.SelectedPot.SelectedIndexChanged += new System.EventHandler(this.SelectedPot_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sensor:";
            // 
            // BeginCal
            // 
            this.BeginCal.Enabled = false;
            this.BeginCal.Location = new System.Drawing.Point(470, 194);
            this.BeginCal.Name = "BeginCal";
            this.BeginCal.Size = new System.Drawing.Size(85, 29);
            this.BeginCal.TabIndex = 2;
            this.BeginCal.Text = "Done";
            this.BeginCal.UseVisualStyleBackColor = true;
            this.BeginCal.Click += new System.EventHandler(this.BeginCal_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(370, 194);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(85, 29);
            this.Cancel.TabIndex = 3;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // Instructions
            // 
            this.Instructions.BackColor = System.Drawing.SystemColors.Control;
            this.Instructions.ForeColor = System.Drawing.Color.Blue;
            this.Instructions.Location = new System.Drawing.Point(16, 47);
            this.Instructions.Multiline = true;
            this.Instructions.Name = "Instructions";
            this.Instructions.Size = new System.Drawing.Size(538, 141);
            this.Instructions.TabIndex = 4;
            this.Instructions.Text = "Place moisture sensor in glass of water filled to line on sensor.\r\n\r\nDo not get c" +
    "ircuitry wet.\r\n\r\nOnce \'Reading\' stabilizes, press \'Done\'";
            // 
            // Reading
            // 
            this.Reading.AutoSize = true;
            this.Reading.Location = new System.Drawing.Point(12, 198);
            this.Reading.Name = "Reading";
            this.Reading.Size = new System.Drawing.Size(116, 20);
            this.Reading.TabIndex = 6;
            this.Reading.Text = "Reading: ???";
            // 
            // CalibrateMoistureSensor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(566, 227);
            this.ControlBox = false;
            this.Controls.Add(this.Reading);
            this.Controls.Add(this.Instructions);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.BeginCal);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SelectedPot);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "CalibrateMoistureSensor";
            this.Text = "Calibrate Moisture Sensor";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox SelectedPot;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BeginCal;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.TextBox Instructions;
        private System.Windows.Forms.Label Reading;
    }
}