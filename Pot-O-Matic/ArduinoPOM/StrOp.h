//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#pragma once

class StrOp
{
public:
	static bool isDigit(const char ch) { return (((ch) >= '0' && (ch) <= '9')); }
	static bool isNumeric(const char *pa) { return (StrOp::isDigit(*pa) || (*pa == '-' && StrOp::isDigit(pa[1]))); }
    static bool isHex(const char c) { return (StrOp::isDigit(c) || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f')); }
    static bool isAlphaNumeric(const char c) { return (StrOp::isDigit(c) || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')); }

	static const char True[];
	static const char False[];
	static const char Unknown[];

	static void reverse(char *s);
	static int nuxi(const char *pa);
    static char *NextToken(char *src);
	static int utoa(ULONG n, char *s);
	static void Clear(char *s, int len);
    static char *efree(const char *src);
	static const char *TrimEnd(char *s);
    static long Numeric(const char *pa);
	static char *NextSubField(char *pf);
    static bool allDigits(const char *s);
    static int Copy(char *d, const char *s);
	static char *SkipWhite(const char *src);
    static int IntToString(char *s, long n);
    static int First(char ch, const char *s);
    static bool Contains(char ch, const char *s);
	static const char *denuxi(char *dest, int cmd);
    static int Copy(char *d, const char *s, int max);
	static char *itoh(unsigned int n, char *s, int w);
    static int Find(const char *pa, const char *pattern);
	static unsigned int htoi(const char *s, int len = 8);
	static const char *CmdToResp(char *dest, short type);
	static int Which(const char *target, const char **list);
    static char *replace(const char *src, char *dest = NULL);
    static bool StartsWith(const char *pa, const char *pattern);
	static int strcasecmp(const char *s1, const char *s2, int count = 0);
	static char *NextSubCommand(char*& msg, short& subType, bool nulTerminate = true);

	static inline char toupper(char c) { return (c >= 'a' && c <= 'z') ? (c & 0x5f) : c; }
	static inline char tolower(char c) { return (c >= 'A' && c <= 'Z') ? (c | 0x20) : c; }

private:
	StrOp() { }
};
