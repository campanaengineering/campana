//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#pragma once

class AirQualitySens : public Ticker
{
public:
  AirQualitySens() : Ticker("AirQuality", 1, OE_Late), hostDirty(false) { }

  void SetHumidity(USHORT val);

private:
  virtual void Tick();
  virtual void Initialize();
  virtual void SystemEvent(SystemEventType type, int n);

  bool hostDirty;

  Characteristic *blueCO2;
  Characteristic *blueTVOC;
  uint16_t co2, last_reported_co2;
  uint16_t tvoc, last_reported_tvoc;
};

extern AirQualitySens AirQuality;
