//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#ifndef HOST_COMMAND_H
#define HOST_COMMAND_H

extern bool SendIdCommand;

/// Class to provide who executes commands from host.
class HostCommand : public Obj
{
public:
  HostCommand(const char *cmd, Str *(*cmdFunc)(char *));

  static void Execute(Str *pCmd);

private:
  static List *list;					///< List of initializers (has to be pointer due to constructor order issues)

  short command;                        ///< Command type to be processed
  Str *(*func)(char *);					///< Pointer to command function
};

#endif
