﻿
namespace Pot_O_Matic_Interface
{
    partial class ChangeNames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Pot1 = new System.Windows.Forms.TextBox();
            this.Pot1Label = new System.Windows.Forms.Label();
            this.Pot2Label = new System.Windows.Forms.Label();
            this.Pot2 = new System.Windows.Forms.TextBox();
            this.OK = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.DeviceLabel = new System.Windows.Forms.Label();
            this.DeviceName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // Pot1
            // 
            this.Pot1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pot1.Location = new System.Drawing.Point(81, 50);
            this.Pot1.Margin = new System.Windows.Forms.Padding(5);
            this.Pot1.Name = "Pot1";
            this.Pot1.Size = new System.Drawing.Size(473, 26);
            this.Pot1.TabIndex = 0;
            // 
            // Pot1Label
            // 
            this.Pot1Label.AutoSize = true;
            this.Pot1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pot1Label.Location = new System.Drawing.Point(10, 53);
            this.Pot1Label.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Pot1Label.Name = "Pot1Label";
            this.Pot1Label.Size = new System.Drawing.Size(61, 20);
            this.Pot1Label.TabIndex = 1;
            this.Pot1Label.Text = "Pot#1:";
            // 
            // Pot2Label
            // 
            this.Pot2Label.AutoSize = true;
            this.Pot2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pot2Label.Location = new System.Drawing.Point(10, 89);
            this.Pot2Label.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.Pot2Label.Name = "Pot2Label";
            this.Pot2Label.Size = new System.Drawing.Size(61, 20);
            this.Pot2Label.TabIndex = 3;
            this.Pot2Label.Text = "Pot#2:";
            // 
            // Pot2
            // 
            this.Pot2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Pot2.Location = new System.Drawing.Point(81, 86);
            this.Pot2.Margin = new System.Windows.Forms.Padding(5);
            this.Pot2.Name = "Pot2";
            this.Pot2.Size = new System.Drawing.Size(473, 26);
            this.Pot2.TabIndex = 2;
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(470, 126);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(84, 31);
            this.OK.TabIndex = 4;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(369, 126);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(84, 31);
            this.Cancel.TabIndex = 5;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // DeviceLabel
            // 
            this.DeviceLabel.AutoSize = true;
            this.DeviceLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviceLabel.Location = new System.Drawing.Point(10, 17);
            this.DeviceLabel.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.DeviceLabel.Name = "DeviceLabel";
            this.DeviceLabel.Size = new System.Drawing.Size(68, 20);
            this.DeviceLabel.TabIndex = 7;
            this.DeviceLabel.Text = "Device:";
            // 
            // DeviceName
            // 
            this.DeviceName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DeviceName.Location = new System.Drawing.Point(81, 14);
            this.DeviceName.Margin = new System.Windows.Forms.Padding(5);
            this.DeviceName.Name = "DeviceName";
            this.DeviceName.Size = new System.Drawing.Size(473, 26);
            this.DeviceName.TabIndex = 6;
            // 
            // ChangeNames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 168);
            this.ControlBox = false;
            this.Controls.Add(this.DeviceLabel);
            this.Controls.Add(this.DeviceName);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.Pot2Label);
            this.Controls.Add(this.Pot2);
            this.Controls.Add(this.Pot1Label);
            this.Controls.Add(this.Pot1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "ChangeNames";
            this.Text = "Change Names";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Pot1;
        private System.Windows.Forms.Label Pot1Label;
        private System.Windows.Forms.Label Pot2Label;
        private System.Windows.Forms.TextBox Pot2;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.Label DeviceLabel;
        private System.Windows.Forms.TextBox DeviceName;
    }
}