//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#include "GalTam.h"

Logger Paul;
Queue Logger::LogQ;
int Logger::pending = 0;
bool VerboseLogging = false;

/// Write pending log items to host
void Logger::Tick()
  {
  if (HostProtocol::Communicating() && LogQ.Entries())
    {
    pending--;
    HostProtocol::Send((Str *)LogQ.Dequeue());
    }
  }

static char logScr[200];

/// Log a string
/// \return True
bool Logger::LogLine(
    LogColor lc,
    bool big,
    const char *format,			                ///< Format of string to log
    ...)
  {
  char *ps = logScr;
  const char *color = "black";

  switch (lc)
    {
    case LC_Green:
      color = "green";
      break;

    case LC_Orange:
      color = "orange";
      break;

    case LC_Red:
      color = "red";
      break;

    case LC_Blue:
      color = "blue";
      break;
    }

  ps += sprintf(ps, "LG|CO%s", color);
  if (big)
    ps += sprintf(ps, "|BG");
  ps += sprintf(ps, "|TX");

  if (StrOp::Contains('%', format))
    {
    va_list args;

    va_start(args, format);
    vsprintf(ps, format, args);
    va_end(args);
    }
  else
    strcpy(ps, format);

  if (pending < 10)
    {
    pending++;
    LogQ.Enqueue(Str::Alloc(logScr));
    }
  return true;
  }

Str *Logger::LV_Func(char *msg)
  {
  VerboseLogging = (msg[2] == '1');
  return NULL;
  }
