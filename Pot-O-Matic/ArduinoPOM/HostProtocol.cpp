/*
**  HostProtocol.cpp - Implementation of the class HostProtocol
**
**
*/

#include "GalTam.h"

HostProtocol PCHost;

Queue HostProtocol::TxToHost;
Queue HostProtocol::RxFromHost;
bool HostProtocol::m_Communicating = false;

//######################### HostProtocol methods ########################################

// CHostProtocol Constructor 
HostProtocol::HostProtocol() : Ticker("HostProtocol", HOST_TPS),
		m_RxCnt(0),
		m_State(HPS_WaitSTX),
        GD_Cmd("GD", GD_Func),
		m_LastComm(SEVEN_YEARS)
  {
  }

/*
**  Initialize - Time zero initialization
**
*/

HardInit *HostProtocol::hInit = new HardInit(HardwareInitialize, OE_First);
void HostProtocol::HardwareInitialize()
  {
  Serial.begin(115200);
  }

/*
**  Poll - Process any received characters
**
*/

void HostProtocol::Poll()
  {
  while (Serial.available())
    {
    UC chr = Serial.read();

    if (chr == ENQ)
      {
      m_LastComm = 0;
      m_Communicating = true;
      if (!m_Ping.InSomeList())
        RxFromHost.Enqueue(&m_Ping);
      }
    else
      switch (m_State)
        {
        case HPS_WaitSTX:
          m_RxCnt = 0;

          if (chr == STX)
            {
            m_LastComm = 0;
            m_State = HPS_GetETX;                     // Start of data (STX not included in buffer)
            m_Communicating = true;
            }
          break;

        case HPS_GetETX:
          if (chr == ETX)
            {
            m_LastComm = 0;
            m_State = HPS_WaitSTX;
            m_RxBuf[m_RxCnt] = '\0';
            RxFromHost.Enqueue(Str::Alloc(m_RxBuf));
            }
          else if (chr == STX)
            {
            m_RxCnt = 0;
            m_LastComm = 0;
            }
          else if (m_RxCnt >= HOST_MAX_RX_SIZE)
            m_State = HPS_WaitSTX;
          else
            m_RxBuf[m_RxCnt++] = chr;
          break;
        }
    }

  if (TxToHost.Entries() && m_Communicating && m_State == HPS_WaitSTX)
    {
    Str *pMsg = (Str *)TxToHost.Dequeue();
    const char *pTx = pMsg->Value();

    Serial.write(STX);
    Serial.write(pTx, strlen(pTx));
    Serial.write(ETX);
    pMsg->Done();
    }
  }

/*
**  Tick - One quarter second has passed, do anything???
**
*/

void HostProtocol::Tick()
  {
  if (m_Communicating && ++m_LastComm > SEVEN_YEARS)
    {
    TxToHost.Empty();
    m_Communicating = false;
    }

  if (m_State == HPS_GetETX && m_LastComm >= 2)
    m_State = HPS_WaitSTX;         // Too long since received STX without receiving ETX, abort packet
  }

/*
**  TxChar - Transmit single character
*/

void HostProtocol::Send(Str *msg)
  {
  if (msg)
    if (m_Communicating)
      TxToHost.Enqueue(msg);
    else
      msg->Done();
  }

Obj *HostProtocol::Receive()
  {
  return RxFromHost.Dequeue();
  }

Str *HostProtocol::GD_Func(char *msg)
  {
  PCHost.m_LastComm = SEVEN_YEARS;        // Received command indicating PCHost going down
  return NULL;
  }
