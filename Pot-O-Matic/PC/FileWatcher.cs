﻿#region Using

using System;
using System.IO;
using System.Threading;

using Utility;

#endregion

namespace Pot_O_Matic_Interface
{
    /// <summary>
    /// Class to monitor filesystem for changes in Funnel managed binaries
    /// </summary>
    public class FileWatcher : IDisposable
    {
        #region Variables

        /// <summary>
        /// Information about all file types
        /// </summary>
        private FileInfo _fileInfo = null;

        /// <summary>
        /// Protocol events sent here
        /// </summary>
        public event FileChangedEventDelegate FileChangedEvent;

        /// <summary>
        /// Delegate for Protocol events
        /// </summary>
        /// <param name="logMess">Message to log</param>
        public delegate void FileChangedEventDelegate(string logMess);

        /// <summary>
        /// Keep track of event handler
        /// </summary>
        private FileSystemEventHandler _watchFileHandler = null;

        /// <summary>
        /// Watch for changes to downloadable files
        /// </summary>
        private static FileSystemWatcher _watchFileChanges = null;

        /// <summary>
        /// Keep track of event handler
        /// </summary>
        private FileSystemEventHandler _watchDirHandler = null;

        /// <summary>
        /// Watch for changes to downloadable files
        /// </summary>
        private static FileSystemWatcher _watchDirChanges = null;

        /// <summary>
        /// Keeps track of when need for update has been logged.
        /// </summary>
        private static bool _updateNeedLogged = false;

        /// <summary>
        /// File extensions for downloaded file types
        /// </summary>
        private static string _filePattern = "Pot-O-Matic*.POM";

        #endregion

        #region Startup/Shutdown

        /// <summary>
        /// Contructor for FileWatcher
        /// </summary>
        public FileWatcher()
        {
            Directory.CreateDirectory(Parse.FirmwarePath);

            UpdateFileInfo();

                        // Watch for changes to the directory itself
            _watchDirChanges = new FileSystemWatcher();
            _watchDirChanges.Path = Parse.LocalPath();
            _watchDirChanges.Filter = "Pot_O_Data";
            _watchDirChanges.NotifyFilter = NotifyFilters.LastWrite;
            _watchDirHandler = new FileSystemEventHandler(WatchFileChanges_Changed);
            _watchDirChanges.Changed += _watchDirHandler;
            _watchDirChanges.EnableRaisingEvents = true;

                        // Watch for changes to files within the directory
            _watchFileChanges = new FileSystemWatcher();
            _watchFileChanges.Path = Parse.FirmwarePath;
            _watchFileChanges.Filter = string.Empty;
            _watchFileChanges.NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastWrite | NotifyFilters.Size;
            _watchFileHandler = new FileSystemEventHandler(WatchFileChanges_Changed);
            _watchFileChanges.Changed += _watchFileHandler;
            _watchFileChanges.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Destructor for FileWatcher
        /// </summary>
        ~FileWatcher()
        {
            Dispose();
        }

        /// <summary>
        /// Dispose of object
        /// </summary>
        public void Dispose()
        {
            try
            {
                _watchDirChanges.Changed -= _watchDirHandler;
                _watchFileChanges.Changed -= _watchFileHandler;
            }
            catch (Exception)
            {
            }
        }

        #endregion

        #region Update version info

        /// <summary>
        /// Update file info for all types
        /// </summary>
        /// <returns>True if at least one type changed</returns>
        public bool UpdateFileInfo()
        {
            long modTime = 0;
            bool updated = false;
            string fname = FindDownloadable();
            FileInfo current = _fileInfo;           // Get current file info

            if (fname != string.Empty)
            {
                modTime = File.GetLastWriteTime(fname).Ticks;

                if (current == null || modTime != current.ModTime)
                {
                    updated = true;
                    _fileInfo = new FileInfo(fname, modTime);
                    Log(string.Format("Update ready for Pot-O-Matic: {0}", fname.Substring(fname.LastIndexOf('\\') + 1)));
                }
            }
            else
            {
                if (current != null)
                    Log("No longer unique download file for Pot-O-Matic");
                _fileInfo = null;
            }

            return updated;
        }

        /// <summary>
        /// Find binary matching file type on disk
        /// </summary>
        /// <param name="type">File type</param>
        /// <returns>File name.</returns>
        private string FindDownloadable()
        {
            try
            {
                string[] files = Directory.GetFiles(Parse.FirmwarePath, _filePattern, SearchOption.TopDirectoryOnly);

                if (files.Length == 0)
                    return string.Empty;     // Not there

                if (files.Length == 2 && _fileInfo != null)
                {               // New file copied in.  Delete old and report new
                    string newFile = string.Empty;
                    string oldFile = _fileInfo.Name;

                    if (files[0] == oldFile)
                        newFile = files[1];
                    else if (files[1] == oldFile)
                        newFile = files[0];

                    if (newFile != string.Empty)
                    {
                        try
                        {
                            File.SetAttributes(oldFile, File.GetAttributes(oldFile) & ~FileAttributes.ReadOnly);
                            File.Delete(oldFile);
                            _fileInfo = null;
                            return newFile;
                        }
                        catch (Exception)
                        {
                        }
                    }
                }

                if (files.Length > 1)
                    return string.Empty;

                return files[0];
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// File last modified time
        /// </summary>
        public long FileTime
        {
            get
            {
                FileInfo info = _fileInfo;

                if (info != null)
                    return info.ModTime;

                return 0;
            }
        }

        /// <summary>
        /// File name of current downloadable for file type
        /// </summary>
        /// <param name="type">Device type</param>
        /// <returns>File name</returns>
        public string DownloadName
        {
            get
            {
                FileInfo info = _fileInfo;

                if (info != null)
                    return info.Name;

                return string.Empty;
            }
        }

        /// <summary>
        /// Given device type, check if need to download updated version
        /// </summary>
        /// <param name="type">Device type</param>
        /// <returns>True if need to update</returns>
        public bool NeedToUpdate()
        {
            bool update = false;

            if (_fileInfo != null && ArduinoCom.Instance.Version != string.Empty && Executive.VersionID != Executive.LastVersionID)
                update = true;

            if (update && !_updateNeedLogged)
            {
                _updateNeedLogged = true;
                Log("Pot-O-Matic is out of date.");
            }

            return update;
        }

        /// <summary>
        /// Report when a file has been downloaded
        /// </summary>
        /// <param name="type">Type of file</param>
        public void FileDownloadedSuccessfully()
        {
            _fileInfo.FileDownloadedSuccessfully();
        }

        #endregion

        #region Events

        /// <summary>
        /// Event called when changes to any files in Resources directory.
        ///     Check if any downloadable files changed and if so, notify receivers
        /// </summary>
        /// <param name="sender">The sender</param>
        /// <param name="e">The event args</param>
        void WatchFileChanges_Changed(object sender, FileSystemEventArgs e)
        {
            if (UpdateFileInfo() && FileChangedEvent != null)
            {
                Thread.Sleep(3000);
                FileChangedEvent(null);
            }
        }

        /// <summary>
        /// Log a message
        /// </summary>
        /// <param name="mess">Message to log</param>
        private void Log(string mess)
        {
            if (FileChangedEvent != null)
                FileChangedEvent(mess);
        }

        #endregion
    }
}
