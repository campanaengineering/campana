//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#ifndef LIST_H
#define LIST_H

/// Class to manage a list of Items
class List
{
public:
	List() : head(NULL) { }

	virtual ~List() { }

	/// First item in list
	/// \return First item in list
	Obj *First() { return head; }

	/// Next item in list or first if passed item is NULL
	/// \return Next item in list or first if passed item is NULL
	Obj *Next(Obj *pItem = NULL) { return (pItem) ? pItem->next : head; }

	/// Empty the list
	virtual void Empty() { while (head) delete TakeFirst(); }

	int Count();
	Obj *TakeLast();
	Obj *TakeFirst();
	Obj *Take(Obj *pTake);
	bool InList(Obj *pItem);
	Obj *AddLast(Obj *pItem);
	Obj *AddFirst(Obj *pItem);
	Obj *AddInOrder(Obj *pItem);

protected:
	Obj *head;					///< Head of list of items
};

/// Provide blocking queue for a list of items
class Queue : protected List
{
public:
	/// Constructor for queue
	Queue() : List() {}

	Obj *Enqueue(Obj *pItem);
	Obj *Dequeue();

	/// Find out if there are entries in queue
	/// /return True if entries present
	inline bool Entries() { return head != NULL; }

	/// Empty the queue
    virtual void Empty() { while (Entries()) delete Dequeue(); }

protected:
	virtual void Add(Obj *pItem) { AddLast(pItem); }
};

#endif
