//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#include "GalTam.h"
#include "Version.h"

#define VALID_VALUE 0x1234

Configuration Config;
EepromContents Configuration::eeprom;

HardInit *Configuration::hInit = new HardInit(HardwareInitialize);
void Configuration::HardwareInitialize()
  {
  for (UC *pb = (UC *)0x10000; pb < (UC *)&__etext; )
    Checksum += *pb++;

  EEPROM.init();
  EEPROM.get(0, eeprom);
  if (eeprom.magicNumber != VALID_VALUE)
    {
    eeprom.magicNumber = VALID_VALUE;
    StrOp::Copy(eeprom.deviceName, SYSTEM_NAME, sizeof(eeprom.deviceName));
    for (int p = 0; p < MAX_POT; p++)
      {
      eeprom.waterDuration[p] = 2;          // Default watering duration
      eeprom.wateringInterval[p] = 60;      // Default watering interval
      eeprom.mositureThreshold[p] = 0;      // Inidcate unset.  The type of moisture sensor class will set this correctly
      eeprom.mositureWetReading[p] = 0;     // Inidcate unset.  Reading of moisture sensor when wet
      eeprom.mositureDryReading[p] = 0;     // Inidcate unset.  Reading of moisture sensor when dry
      sprintf(eeprom.potName[p], "Pot #%d", p + 1);
      }
    EEPROM.put(0, eeprom);
    }
  }

void Configuration::DoSet(SystemEventType type, int n)
  {
  EEPROM.put(0, eeprom);
  Ticker::SendSystemEvent(type, n);
  }
