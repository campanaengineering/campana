/*
**  HostProtocol.h - Definition of Arduino protocol class
**
*/

#ifndef HOST_PROTOCOL_H
#define HOST_PROTOCOL_H

#define STX                     2
#define ETX                     3
#define ENQ                     5

#define HOST_MAX_RX_SIZE	    (1 K)
#define HOST_MAX_TX_SIZE	    (1 K)

#define	HOST_TPS			    4
#define	SEVEN_YEARS			    (30 * HOST_TPS)

enum HostProtocolState
  {
  HPS_WaitSTX,
  HPS_GetETX,
  };

class HostProtocol : public Ticker
  {
  public:
	HostProtocol();

	static Obj *Receive();
    static void Send(Str *msg);
    static void Send(const char *msg) { Send(Str::Alloc(msg)); }
	static inline bool Communicating() { return m_Communicating; }

  private:
    ULONG m_RxCnt;                              // Count of packet characters received
    KeptObj m_Ping;                             // Passed to indicate a ping received
	ULONG m_LastComm;						    // Time since last communication with host occurred
    HostCommand GD_Cmd;
	HostProtocolState m_State;					// Current state of protocol
    char m_RxBuf[HOST_MAX_RX_SIZE + 1];			// Receive buffer

    static Queue TxToHost;                      // Messages to be sent to host
    static Queue RxFromHost;                    // Messages received from host to be processed
    static Str statusResponse;                  ///< Response to send when ENQ received
	static bool m_Communicating;
    static Str *GD_Func(char *msg);

  protected:
    virtual void Tick();
    virtual void Poll();

    static HardInit *hInit;                     // Hardware initialization
    static void HardwareInitialize();
};

#endif
