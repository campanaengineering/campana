﻿using System;
using System.Windows.Forms;

namespace Pot_O_Matic_Interface
{
    public partial class ChangeNames : Form
    {
        public ChangeNames()
        {
            InitializeComponent();

            Pot1.Text = UI.Pot[0].PotGroup.Text;
            Pot2.Text = UI.Pot[1].PotGroup.Text;
            DeviceName.Text = UI.Instance.DeviceName.Text;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (Pot1.Text.Length != 0 && Pot1.Text != UI.Pot[0].PotGroup.Text)
                UI.POM.SetPotName(0, Pot1.Text);
            if (Pot2.Text.Length != 0 && Pot2.Text != UI.Pot[1].PotGroup.Text)
                UI.POM.SetPotName(1, Pot2.Text);
            if (DeviceName.Text.Length != 0 && DeviceName.Text != UI.Instance.DeviceName.Text)
                UI.POM.SetDeviceName(DeviceName.Text);
            Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
