//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#ifndef TICKER_H

//******************************************** Ticker Declaration *********************************************

// This is the main vehicle for getting access to CPU time.
// Arduino systems are singly threaded (with a few interrupts).
// If classes inherit from this, they will be added to a list of classes that will be called
//      * Once during initialization
//      * Periodically (time variable)
//      * Polled at high speed (once per Arduino loop)
//      * When certain system events occur

// Classes provide override functions for each of these situations.
// Initialization can also be directed to occur in a specified order by indicating when the intializer should be called relative to one another

class Ticker : public Obj
{
public:
  Ticker(const char *nme, int tps = -1, OrderOfExecution pri = OE_Middle);

  static void PollClients();
  static void PerformInitialization();
  static void SendSystemEvent(SystemEventType type, int n = -1);

/// Get type of object
/// \return Object type
  virtual ObjType GetObjType() { return Type_Ticker; }

protected:
  int interval;

  virtual void Tick() {}
  virtual void Poll() {}
  virtual void Initialize() {}
  virtual void SystemEvent(SystemEventType type, int n) {}

  void RemoveClient() { tickers->Take(this); }

  virtual bool InsertHere(Obj *inList) { return order < ((Ticker *)inList)->order; }

private:
  char name[20];
  ULONG nextTick;
  OrderOfExecution order;

  static List *tickers;
};

//******************************************** HardInit Declaration *********************************************

/// Class to provide early initialization of hardware systems.
/// Sometimes there is hardware that is used by several subsystems that needs to be initialized first
/// This is designed for those situations.  Generally just hardware initialization goes in these functions.
/// This set of initializations is called prior to any Ticker initializations.

class HardInit : public Obj
{
public:
  HardInit(void (*initFunc)(), OrderOfExecution ord = OE_Late);
  virtual ~HardInit() { }

  static void PerformInitialization();

private:
  static List *list;                        ///< List of initializers (has to be pointer due to constructor order issues)

  void (*func)();                           ///< Pointer to initialization function
  OrderOfExecution order;                   ///< Point during initialization that this function should be called

protected:
  virtual bool InsertHere(Obj *inList) { return order < ((HardInit *)inList)->order; }
};

#endif
#define TICKER_H
