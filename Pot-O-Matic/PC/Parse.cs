﻿#region Using

using System;
using System.IO;
using System.Text;
using Microsoft.Win32;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Runtime.InteropServices;

#endregion

namespace Utility
{
    /// <summary>
    /// Class used to parse messages
    /// </summary>
    public class Parse
    {
        #region Static Functions

        /// <summary>
        /// Return numeric value of string ignoring trailing non-digits
        /// </summary>
        /// <param name="str">String to examine</param>
        /// <returns>Numeric value of leading digits</returns>
        public static int AtoI(string str)
        {
            int num = 0;

            if (str != null && str.Length > 0)
            {
                int i = 0;
                bool neg = false;

                if (str[0] == '-')
                {
                    i++;
                    neg = true;
                }

                for (; i < str.Length; i++)
                {
                    char c = str[i];

                    if (!Char.IsDigit(c))
                        break;

                    num = (num * 10) + (c - '0');
                }

                if (neg)
                    num = -num;
            }
            return num;
        }

        /// <summary>
        /// Numeric equivalent of field type
        /// </summary>
        /// <param name="msg">Message field</param>
        /// <returns>Numeric equivalent of field type</returns>
        public static int TypeInt(string msg)
        {
            if (msg.Length >= 2)
                return ((Byte)msg[1] << 8) + (Byte)msg[0];
            return 0;
        }

        /// <summary>
        /// Split a message into its fields
        /// </summary>
        /// <param name="msg">The message</param>
        /// <returns>List of fields</returns>
        public static List<Field> Fields(string msg)
        {
            int index;
            List<Field> fields = new List<Field>();

            if (msg != null)
            {
                index = msg.IndexOf('/');
                if (index >= 0)
                    msg = msg.Substring(index + 1);

                while (msg.Length >= 2)
                {
                    index = msg.IndexOf('|');
                    if (index < 0)
                    {
                        fields.Add(new Field(msg));
                        break;
                    }
                    if (index < 2)
                        break;

                    fields.Add(new Field(msg.Remove(index)));
                    msg = msg.Substring(index + 1);
                }
            }

            return fields;
        }

        #endregion

        #region File Path variables

        /// <summary>
        /// Expand to full path of file in executing assembly's path 
        /// </summary>
        /// <param name="filename">Filename to put at end of path</param>
        /// <returns>Executing assembly's path with optional filename appended</returns>
        public static string LocalPath(string filename = "")
        {
            if (_localPath == null)
            {
                _localPath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                _localPath = _localPath.Remove(_localPath.LastIndexOf('\\')) + @"\";
            }

            return _localPath + filename;
        }

        private static string _localPath = null;

        /// <summary>
        /// Devices data path below full path of file in executing assembly's path 
        /// </summary>
        public static string DataPath
        {
            get
            {
                if (_dataPath == null)
                {
                    _dataPath = LocalPath();
                    _dataPath += @"Pot_O_Data\";
                }
                return _dataPath;
            }
        }

        private static string _dataPath = null;

        /// <summary>
        /// The directory where current firmware versions are stored
        /// </summary>
        public static readonly string FirmwarePath = DataPath;

        /// <summary>
        /// The filename of where variables are persisted
        /// </summary>
        public static readonly string PersistedFileName = DataPath + "Pot_O_Matic.ini";

        /// <summary>
        /// Path containing file
        /// </summary>
        /// <param name="fname"></param>
        /// <returns></returns>
        public static string FileHead(string fname)
        {
            return fname.Substring(0, fname.LastIndexOf('\\'));
        }

        /// <summary>
        /// File name extracted from path
        /// </summary>
        /// <param name="fname"></param>
        /// <returns></returns>
        public static string FileTail(string fname)
        {
            return fname.Substring(fname.LastIndexOf('\\') + 1);
        }

        #endregion

        #region Persisted variables

        /// <summary>
        /// Save a variable
        /// </summary>
        /// <param name="section">Which section</param>
        /// <param name="key">Keyname within section</param>
        /// <param name="value">Value to set variable to</param>
        public static string SetPersistedVariable(string section, string key, string value)
        {
            WritePrivateProfileString(section, key, value, PersistedFileName);
            return value;
        }

        /// <summary>
        /// Save a variable
        /// </summary>
        /// <param name="type">Device type of variable</param>
        /// <param name="key">Keyname within section</param>
        /// <param name="value">Value to set variable to</param>
        public static int SetPersistedVariable(string section, string key, int value)
        {
            SetPersistedVariable(section, key, value.ToString());
            return value;
        }

        /// <summary>
        /// Save a variable
        /// </summary>
        /// <param name="type">Device type of variable</param>
        /// <param name="key">Keyname within section</param>
        /// <param name="value">Value to set variable to</param>
        public static double SetPersistedVariable(string section, string key, double value)
        {
            SetPersistedVariable(section, key, value.ToString("F3"));
            return value;
        }

        /// <summary>
        /// Read a variable
        /// </summary>
        /// <param name="section">Which section</param>
        /// <param name="key">Keyname within section</param>
        /// <param name="defaultValue">If variable not present, use this value</param>
        /// <returns>String containing variable value</returns>
        public static string GetPersistedVariable(string section, string key, string defaultValue = "")
        {
            StringBuilder result = new StringBuilder(255);

            GetPrivateProfileString(section, key, defaultValue, result, 255, PersistedFileName);
            return result.ToString();
        }

        /// <summary>
        /// Read a variable
        /// </summary>
        /// <param name="section">Which section</param>
        /// <param name="key">Keyname within section</param>
        /// <param name="defaultValue">If variable not present, use this value</param>
        /// <returns>Int containing variable value</returns>
        public static int GetPersistedVariable(string section, string key, int defaultValue = -1)
        {
            StringBuilder result = new StringBuilder(255);

            GetPrivateProfileString(section, key, defaultValue.ToString(), result, 255, PersistedFileName);
            return AtoI(result.ToString());
        }

        /// <summary>
        /// Read a variable
        /// </summary>
        /// <param name="type">Device type of variable</param>
        /// <param name="key">Keyname within section</param>
        /// <param name="defaultValue">If variable not present, use this value</param>
        /// <returns>Int containing variable value</returns>
        public static double GetPersistedVariable(string section, string key, double defaultValue)
        {
            double rc = 0;

            try
            {
                rc = double.Parse(GetPersistedVariable(section, key, defaultValue.ToString()));
            }
            catch (Exception)
            {
                rc = 0;
            }
            return rc;
        }

        #endregion

        #region C++ Imports

        /// <summary>
        /// Get INI file string
        /// </summary>
        /// <param name="section">Section name.</param>
        /// <param name="key">Key within section.</param>
        /// <param name="def">Default value if key not present.</param>
        /// <param name="retVal">Key value.</param>
        /// <param name="size">Max length of string.</param>
        /// <param name="filePath">Path to INI file.</param>
        /// <returns>Length of key.</returns>
        [DllImport("kernel32")]
        public static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        /// <summary>
        /// Write INI file string
        /// </summary>
        /// <param name="section">Section name.</param>
        /// <param name="key">Key within section.</param>
        /// <param name="value">Key value.</param>
        /// <param name="filePath">Path to INI file.</param>
        /// <returns>True on success.</returns>
        [DllImport("kernel32")]
        public static extern bool WritePrivateProfileString(string section, string key, string value, string filePath);

        #endregion
    }

    #region Internal Classes

    /// <summary>
    /// A field within a message
    /// </summary>
    public class Field
    {
        /// <summary>
        /// Numeric value of first two characters concatenated
        /// </summary>
        public int TypeInt { get; private set; }

        /// <summary>
        /// Type of field (first two characters)
        /// </summary>
        public string Type { get; private set; }

        /// <summary>
        /// Field data
        /// </summary>
        public string Data { get; private set; }

        /// <summary>
        /// Field constructor
        /// </summary>
        /// <param name="field">The field</param>
        public Field(string field)
        {
            Data = field.Substring(2);
            Type = field.Substring(0, 2);
            TypeInt = Parse.TypeInt(field);
        }
    }

    #endregion
}
