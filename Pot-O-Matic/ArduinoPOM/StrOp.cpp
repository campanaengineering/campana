//-----------------------------------------------------------------------
//  Author: Greg Winters
//-----------------------------------------------------------------------

#include "GalTam.h"

char const StrOp::True[] = "True";
char const StrOp::False[] = "False";
char const StrOp::Unknown[] = "???";

/// Return equivalent value of next two characters as a number
int StrOp::nuxi(
    const char *pa			///< String to convert
)
  {
  return (*pa << 8) + pa[1];
  }

/// Perform reverse nuxi
/// \return String containing two character command equivalent
const char *StrOp::denuxi(
    char *dest,			///< Destination string
    int cmd				///< Command to denuxify
)
  {
  dest[0] = (char) (cmd >> 8);
  dest[1] = (char)cmd;
  dest[2] = '\0';
  return dest;
  }

/// Create response type from command type
/// \return String containing two character response type
const char *StrOp::CmdToResp(
    char *dest,			///< Destination string
    short type			///< Command to denuxify
)
  {
  StrOp::denuxi(dest, type);
  dest[0] = tolower(dest[0]);
  dest[1] = tolower(dest[1]);
  dest[2] = '\0';

  return dest;
  }

/// Convert hex string to an unsigned
/// \return Converted number
unsigned int StrOp::htoi(
    const char *s,			///< String to convert
    int len					///< Length to convert
)
  {
  char ch;
  unsigned int n = 0;

  if (len <= 0)
    return 0;
  if (len > 8)
    len = 8;

  while (len--)
    {
    ch = *s++;
    if (isHex(ch))
      {
      if ((ch -= '0') > 9)
        ch = (ch & 0x5f) - 7;
      n = (n << 4) + ch;
      }
    else
      break;
    }
  return n;
  }

/// Convert the integer argument to a hex character field
/// \return End of string created
char *StrOp::itoh(
    unsigned int n,			///< Number to convert
    char *s,				///< Destination string
    int w					///< Width of output
)
  {
  int i, tmp;					// scratch
  char *eos = (s += w);			// end of string

  for (i = 0, *s-- = '\0'; n && i < w; i++, n >>= 4)
    {
    tmp = (n & 0x0f);
    *s-- = tmp + (tmp > 9 ? '7' : '0');
    }

  for (; i < w; i++)
    *s-- = '0';

  return eos;
  }

/// Given a string, convert it to a number.  Will take a hex number (preceded by 0x)
/// \return Converted number
long StrOp::Numeric(
    const char *pa			///< String to convert
)
  {
  long val;
  bool neg = false;

  if (pa == NULL)
    return 0;

  if (*pa == '-')
    {
    pa++;
    neg = true;
    }

  val = (*pa == '0' && pa[1] == 'x') ? htoi(pa + 2) : atoi(pa);

  return (neg) ? -val : val;
  }

/// Reverse a string in place
void StrOp::reverse(
    char *s					///< String to reverse
)
  {
  UC c;

  for (int i = 0, j = strlen(s) - 1; i < j; i++, j--)
    {
    c = s[i];
    s[i] = s[j];
    s[j] = c;
    }
  }

/// Find a target string within a list of possible values (list must be NULL terminated)
int StrOp::Which(const char *target, const char **list)
  {
  for (int i = 0; *list; i++, list++)
    if (strcasecmp(target, *list) == 0)
      return i;
  return -1;
  }

/// Free memory if not in flash
char *StrOp::efree(const char *src)
  {
  if (src && !IN_FLASH(src))
    delete [] src;
  return NULL;
  }

/// Replace character pointer with new string, reusing current string if possible
/// \return New string pointer
char *StrOp::replace(
    const char *src,            ///< String to replace it with
    char *dest                  ///< Current contents of string
)
  {
  int len = strlen(src);

  if (dest && (IN_FLASH(src) || IN_FLASH(dest) || strlen(dest) != len))
    dest = efree(dest);

  if (IN_FLASH(src))
    return (char *)src;

  if (dest == NULL)
    dest = new char[len + 1];

  Copy(dest, src);

  return dest;
  }

/// Copy one string into another
/// \return String length
int StrOp::Copy(
    char *d,                       ///< Destination string
    const char *s                  ///< Source string
)
  {
  return Copy(d, s, 10000);
  }

/// Copy one string into another
/// \return String length
int StrOp::Copy(
    char *d,                        ///< Destination string
    const char *s,                  ///< Source string
    int max                         ///< Maximum string length
)
  {
  int len = 0;

  if (s && d && s != d)
    {
    for (; *s && max-- > 0; len++)
      *d++ = *s++;
    *d = '\0';
    }
  else if (d)
    *d = '\0';

  return len;
  }

void StrOp::Clear(char *s, int len)
  {
  for (int i = 0; i < len; i++)
    *s++ = '\0';
  }

int StrOp::strcasecmp(const char *s1, const char *s2, int count)
  {
  UC c1, c2;

  if (count <= 0)
    count = 100000;

  while (count-- > 0)
    {
    c1 = *s1++;
    c2 = *s2++;

    if (c1 >= 'a' && c1 <= 'z')
      c1 &= 0x5f;
    if (c2 >= 'a' && c2 <= 'z')
      c2 &= 0x5f;

    if (c1 != c2)
      break;

    if (c1 == '\0')
      return 0;
    }
  return (c1 < c2) ? -1 : 1;
  }

/// Convert the argument to a character string
/// \return Length of string
int StrOp::IntToString(
    char *s,                    ///< Destination string
    long num                    ///< Number to convert
)
  {
  char *s1;
  int n = abs(num);

  s1 = s;
  do
    {
    *s++ = (n % 10) + '0'; /* get the next digit */
    } while ( (n /= 10) > 0); /* and delete it */

  if (num < 0)
    *s++ = '-';

  *s = '\0';
  StrOp::reverse(s1); /* reverse s in place */

  return strlen(s1);
  }

/// Convert the unsigned argument to a character string
/// \return Length of string
int StrOp::utoa(
    ULONG n,				///< Number to convert
    char *s					///< Destination string
)
  {
  char *s1;

  s1 = s;
  do
    {
    *s++ = (UCHAR) (n % 10) + '0'; /* get the next digit */
    } while ( (n /= 10) > 0); /* and delete it */

  *s = '\0';
  StrOp::reverse(s1); /* reverse s in place */

  return strlen(s1);
  }

/// Returns true if string contains at least one of the passed character
bool StrOp::Contains(char ch, const char *s)
  {
  while (*s)
    if (*s++ == ch)
      return true;
  return false;
  }

/// Returns index of first instance of character in string (-1 if not found)
int StrOp::First(char ch, const char *s)
  {
  for (int i = 0; *s; i++)
    if (*s++ == ch)
      return i;
  return -1;
  }

/// Returns true if all characters in string are digits
bool StrOp::allDigits(const char *s)
  {
  while (*s)
    if (!isDigit(*s++))
      return false;
  return true;
  }

/// Returns offset into pa where pattern starts, return -1 if pattern not found
int StrOp::Find(const char *pa, const char *pattern)
  {
  for (int off = 0, len = strlen(pattern); len && *pa; off++)
    if (strncmp(pattern, pa + off, len) == 0)
      return off;
  return -1;
  }

/// Returns true if pa starts with pattern
bool StrOp::StartsWith(const char *pa, const char *pattern)
  {
  while (*pa)
    if (*pattern == '\0')
      return true;
    else if (*pa++ != *pattern++)
      break;
  return false;
  }

/// Trims any blanks off of end of string
const char *StrOp::TrimEnd(char *s)
  {
  for (char *t = s + strlen(s) - 1; t != s && *t == ' '; )
    *t-- = '\0';
  return s;
  }

/// Skip over white space
/// \return Next non-white character
char *StrOp::SkipWhite(
    const char *src			///< Starting location
)
  {
  if (src == NULL)
    return NULL;

  while (*src == ' ' || *src == '\t')
    src++;

  if (*src == '\0' || *src == '\n')
    return NULL;

  return (char *)src;
  }

/// Scan to next token
/// \return Start of next token
char *StrOp::NextToken(
    char *src				///< Starting location
)
  {
  bool sawQuote = false;

  if (src == NULL)
    return NULL;

  for (; (*src >= ' ' && *src <= '~') || *src == '\t'; src++)
    {
    if ( (*src == ' ' || *src == '\t') && !sawQuote)
      break;
    else if (*src == '\"')
      sawQuote = !sawQuote;
    }

  return StrOp::SkipWhite(src);
  }

/// Find the next sub-field of in a message
/// \return Pointer to next sub-field
char *StrOp::NextSubField(
    char *pf				///< Current field pointer
)
  {
  if (pf)
    while (*pf)
      if (*pf++ == ',' && *pf)
        {
        pf[-1] = '\0';
        return pf;
        }
  return NULL;
  }

/// Find the next sub-command of in a message
/// \return Pointer to next sub-field
char *StrOp::NextSubCommand(
    char*& msg,				///< Current field pointer
    short& subType,         ///< Will be set to subtype
    bool nulTerminate		///< If True, change '|' to '\0'
)
  {
  if (msg)
    {
    subType = nuxi(msg);
    msg += 2;

    for (char *pf = msg; *pf; )
      if (*pf++ == '|')
        {
        if (nulTerminate)
          pf[-1] = '\0';
        return *pf ? pf : NULL;
        }
    }
  else
    subType = -1;
  return NULL;
  }

