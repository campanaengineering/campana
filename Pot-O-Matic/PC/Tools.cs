﻿#region Using

using System;
using System.IO;
using System.Threading;
using System.Diagnostics;

#endregion

namespace Utility
{
    public class Tools
    {
        /// <summary>
        /// Run process and wait for it to stop or n secs
        /// </summary>
        /// <returns>process exit code or -1 on failure</returns>
        public static int RunProcWithTimeout(string ExeName, string arguments, string workingDir = null, long TimeOutSecs = 30, bool UseShellExecute = true, bool hide = true)
        {
            int exitCode = -1;
            string origWorkingDir = string.Empty;
            Stopwatch stopWatch = new Stopwatch();

            // Prepare the process to run
            ProcessStartInfo start = new ProcessStartInfo();
            // Enter in the command line arguments, everything you would enter after the executable name itself
            start.Arguments = arguments;
            // Enter the executable to run, including the complete path
            start.FileName = ExeName;
            // Set the default Dir for running
            if (!string.IsNullOrEmpty(workingDir))
            {
                start.WorkingDirectory = workingDir;

                origWorkingDir = Directory.GetCurrentDirectory();
                Directory.SetCurrentDirectory(workingDir);
            }

            //true if the shell should be used when starting the process; false if the process should be 
            //created directly from the executable file.The default is true.
            start.UseShellExecute = UseShellExecute;

            if (!string.IsNullOrEmpty(workingDir))
                Directory.SetCurrentDirectory(workingDir);

            // Do you want to show a console window?
            if (hide)
            {
                start.WindowStyle = ProcessWindowStyle.Hidden;
                start.CreateNoWindow = true;
            }
            else
            {
                start.WindowStyle = ProcessWindowStyle.Normal;
                start.CreateNoWindow = false;
            }

            stopWatch.Start();

            // Run the external process & wait for it to finish
            Process proc = Process.Start(start);
            Thread.Sleep(1000); // give cpu back to OS to start the process
            if (proc != null)
            {
                while (true)
                {
                    if (proc.HasExited)
                    {
                        exitCode = proc.ExitCode;       // Retrieve the app's exit code
                        break;
                    }
                    if (TimeOutSecs < stopWatch.ElapsedTicks / Stopwatch.Frequency)
                        break;
                    Thread.Sleep(1000);
                }
            }

            stopWatch.Stop();
            if (!string.IsNullOrEmpty(workingDir))
                Directory.SetCurrentDirectory(origWorkingDir);
            return exitCode;
        }
    }
}
