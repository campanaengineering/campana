#include "GalTam.h"
#include "Moisture.h"
#include "PotConfig.h"

//******************************************** Pump Configuration Support *********************************************

Service *PotService[MAX_POT] = { NULL };

class Pot
{
public:
  Pot(int p) : pot(p)
    {
    PotService[p] = new Service("Pot", p + 1);
    bluePotName = new Characteristic(pot + 1, POT_Name, "Name", PotService[p], BLERead | BLEWrite | BLENotify, Config.GetPotName(pot), BlueUpdatedPotName, this);
    blueWaterDuration = new Characteristic(pot + 1, POT_PumpDuration, "PumpDuration", PotService[p], BLERead | BLEWrite | BLENotify, Config.GetWaterDuration(pot), BlueUpdatedDuration, this);
    blueWateringInterval = new Characteristic(pot + 1, POT_WateringInterval, "WateringInterval", PotService[p], BLERead | BLEWrite | BLENotify, Config.GetWateringInterval(pot), BlueUpdatedInterval, this);
    blueMoistureThreshold = new Characteristic(pot + 1, POT_MoistureThreshold, "MoistureThreshold", PotService[p], BLERead | BLEWrite | BLENotify, Config.GetMoistureThreshold(pot), BlueUpdatedThreshold, this);
    blueMoistureWetReading = new Characteristic(pot + 1, POT_MoistureWetReading, "MoistureWetReading", PotService[p], BLERead | BLEWrite | BLENotify, Config.GetMoistureWetReading(pot), BlueUpdatedWetReading, this);
    blueMoistureDryReading = new Characteristic(pot + 1, POT_MoistureDryReading, "MoistureDryReading", PotService[p], BLERead | BLEWrite | BLENotify, Config.GetMoistureDryReading(pot), BlueUpdatedDryReading, this);
    }

  // Ticks once/sec.
  void Tick()
    {
    if (HostProtocol::Communicating())
      {     // Inform host of any changes it needs to know about
      char scr[50];

      if (potNameDirty)
        {
        potNameDirty = false;
        sprintf(scr, "PN%d|NM%s", pot, Config.GetPotName(pot));
        HostProtocol::Send(scr);
        }

      if (waterDurationDirty)
        {
        waterDurationDirty = false;
        sprintf(scr, "WD%d|NM%d", pot, Config.GetWaterDuration(pot));
        HostProtocol::Send(scr);
        }

      if (wateringIntervalDirty)
        {
        wateringIntervalDirty = false;
        sprintf(scr, "WI%d|NM%d", pot, Config.GetWateringInterval(pot));
        HostProtocol::Send(scr);
        }

      if (moistureThresholdDirty)
        {
        moistureThresholdDirty = false;
        sprintf(scr, "TH%d|NM%d", pot, Config.GetMoistureThreshold(pot));
        HostProtocol::Send(scr);
        }
      }
    }

  // A system event has occurred.  Update data accordingly
  void SystemEvent(SystemEventType type, int n)
    {
    if (type == SE_PC_Reconnect)
      potNameDirty = waterDurationDirty = wateringIntervalDirty = moistureThresholdDirty = true;
    else if (n == pot)
      if (type == SE_PotNameUpdated)
        {
        potNameDirty = true;
        blueWaterDuration->Write(Config.GetWaterDuration(pot));
        }
      else if (type == SE_PumpDurationUpdated)
        {
        waterDurationDirty = true;
        blueWaterDuration->Write(Config.GetWaterDuration(pot));
        }
      else if (type == SE_WateringIntervalUpdated)
        {
        wateringIntervalDirty = true;
        blueWateringInterval->Write(Config.GetWateringInterval(pot));
        }
      else if (type == SE_MoistureThresholdUpdated)
        {
        moistureThresholdDirty = true;
        blueMoistureThreshold->Write(Config.GetMoistureThreshold(pot));
        }
      else if (type == SE_MoistureReadingWet)
        blueMoistureWetReading->Write(Config.GetMoistureWetReading(pot));
      else if (type == SE_MoistureReadingDry)
        blueMoistureDryReading->Write(Config.GetMoistureDryReading(pot));
    }

private:
  int pot;
  bool potNameDirty;
  bool waterDurationDirty;
  bool wateringIntervalDirty;
  bool moistureThresholdDirty;
  Characteristic *bluePotName;
  Characteristic *blueWaterDuration;
  Characteristic *blueWateringInterval;
  Characteristic *blueMoistureThreshold;
  Characteristic *blueMoistureWetReading;
  Characteristic *blueMoistureDryReading;

  static void BlueUpdatedPotName(void *pPot, const char *val) { Config.SetPotName(((Pot *)pPot)->pot, val); }
  static void BlueUpdatedDuration(void *pPot, const char *val) { Config.SetWaterDuration(((Pot *)pPot)->pot, StrOp::Numeric(val)); }
  static void BlueUpdatedInterval(void *pPot, const char *val) { Config.SetWateringInterval(((Pot *)pPot)->pot, StrOp::Numeric(val)); }
  static void BlueUpdatedThreshold(void *pPot, const char *val) { Config.SetMoistureThreshold(((Pot *)pPot)->pot, StrOp::Numeric(val)); }
  static void BlueUpdatedWetReading(void *pPot, const char *val) { Config.SetMoistureWetReading(((Pot *)pPot)->pot, StrOp::Numeric(val)); }
  static void BlueUpdatedDryReading(void *pPot, const char *val) { Config.SetMoistureDryReading(((Pot *)pPot)->pot, StrOp::Numeric(val)); }
};

//******************************************** Watering Support *********************************************
// High level manager for all pumps

class PotConfig : public Ticker
{
public:
  PotConfig() : Ticker("PotConfig", 1, OE_Early), PN_Cmd("PN", PN_Func), WD_Cmd("WD", WD_Func), WI_Cmd("WI", WI_Func), MT_Cmd("MT", MT_Func) { }

protected:
  virtual void Initialize()
    {
    for (int p = 0; p < MAX_POT; p++)
      pot[p] = new Pot(p);
    }

  virtual void Tick()
    {
    for (int p = 0; p < MAX_POT; p++)
      pot[p]->Tick();
    }

  virtual void SystemEvent(SystemEventType type, int n)
    {
    for (int p = 0; p < MAX_POT; p++)
      pot[p]->SystemEvent(type, n);
    }

private:
  // Host updating pot name
  static Str *PN_Func(char *msg)
    {
    short p = -1;
    short subType;

    for (char *pn = StrOp::NextSubCommand(msg, subType); msg; msg = pn, pn = StrOp::NextSubCommand(msg, subType))
      switch (subType)
        {
        case TwoChar('P', 'N'):           // Which pot
          p = atoi(msg);
          break;

        case TwoChar('N', 'M'):           // The new name
          if (p >= 0 && p < MAX_POT)
            Config.SetPotName(p, msg);
          break;
        }

    return NULL;
    }

  // Host updating watering duration configuration
  static Str *WD_Func(char *msg)
    {
    short p = -1;
    short subType;

    for (char *pn = StrOp::NextSubCommand(msg, subType); msg; msg = pn, pn = StrOp::NextSubCommand(msg, subType))
      switch (subType)
        {
        case TwoChar('W', 'D'):           // Which moisture channel
          p = atoi(msg);
          break;

        case TwoChar('N', 'M'):           // The new duration
          if (p >= 0 && p < MAX_POT)
            Config.SetWaterDuration(p, atoi(msg));
          break;
        }

    return NULL;
    }

  // Host updating watering interval configuration
  static Str *WI_Func(char *msg)
    {
    short p = -1;
    short subType;

    for (char *pn = StrOp::NextSubCommand(msg, subType); msg; msg = pn, pn = StrOp::NextSubCommand(msg, subType))
      switch (subType)
        {
        case TwoChar('W', 'I'):           // Which moisture channel
          p = atoi(msg);
          break;

        case TwoChar('N', 'M'):           // The new interval
          if (p >= 0 && p < MAX_POT)
            Config.SetWateringInterval(p, atoi(msg));
          break;
        }

    return NULL;
    }

  static Str *MT_Func(char *msg)
    {
    short subType;
    short p = -1;

    for (char *pn = StrOp::NextSubCommand(msg, subType); msg; msg = pn, pn = StrOp::NextSubCommand(msg, subType))
      switch (subType)
        {
        case TwoChar('M', 'T'):           // Which moisture channel
          p = atoi(msg);
          break;

        case TwoChar('T', 'H'):           // The new threshold
          if (p >= 0 && p < MAX_POT)
            Config.SetMoistureThreshold(p, atoi(msg));
          break;
        }

    return NULL;
    }

  HostCommand PN_Cmd;
  HostCommand WD_Cmd;
  HostCommand WI_Cmd;
  HostCommand MT_Cmd;
  static Pot *pot[MAX_POT];
} pots;

Pot *PotConfig::pot[MAX_POT];
